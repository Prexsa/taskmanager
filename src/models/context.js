import React, {useState, useContext, useEffect, useCallback} from 'react';
import BoardService from '../services/board.service';
import TaskService from '../services/task.service';
import UserService from '../services/user.service';
// new libraries
// https://justjs.github.io/
// http://yargs.js.org/

const AppContext = React.createContext();

const AppProvider = ({ children }) => {
  const [boards, setBoards] = useState([]);
  const [tasks, setTasks] = useState([]);
  const [currentBoard, setCurrentBoard] = useState({
    title: '',
    list: [],
    _id: '',
    collab: [],
    owners: []
  });

  const getBoards = useCallback(userId => {
      UserService.getUserBoards(userId).then(async resp => {
          // console.log('response: ', resp)
          const boards = await resp.data.boards.ownBoards;
          // console.log('boards: ', boards)
          setBoardListDetailsToContext(boards);
        })
    }, [])

  useEffect(() => {
    if(currentBoard._id !== '') {
      TaskService.fetchTasksByBoardId(currentBoard._id).then(resp => {
        // console.log('resp: ', resp)
        setTasks([...resp.data.tasks])
      })
    }
  }, [currentBoard._id])

/*  useEffect(() => {
    console.log({ boards, tasks, currentBoard })
  }, [boards, tasks, currentBoard])*/

  const removeCurrentBoardProfile = () => {
    // setCurrentBoardId('');
    setCurrentBoard({
      title: '',
      list: [],
      _id: ''
    })
  }

  const createNewBoard = async newBoardTitle => {
    const userLS = JSON.parse(localStorage.getItem('user'));
    const userId = userLS.id;
    const {
      data: { user, board }
    } = await BoardService.createBoard({ title: newBoardTitle, userId: userId });
    // console.log('board: ', board)
    const ownBoards = user.ownBoards;
    setCurrentBoard(board);
    setBoardListDetailsToContext(ownBoards);
  }

  const setBoardListDetailsToContext = async boards => {
    const boardList = await Promise.all(
         boards.map(async (board) => {
          const response = await BoardService.getBoardById(board.boardId);
          // console.log('response: ', response)
          return response.data.board;
        })
      );
    const filterUndefined = boardList.filter(x => x !== undefined)
    setBoards([...filterUndefined])
  }

  const currentBoardDetails = boardId => {
    let index = null;
    let board = {};

    for(let [i, board] of boards.entries()) {
      if(board._id === currentBoard._id) {
        board = {...boards[i]};
        index = i
        break;
      }
    }

    return { index, board }
  }

  const updateCurrentBoardProfile = updatedBoard => {
    // console.log('updatedBoard: ', updatedBoard)
    const boardId = updatedBoard._id;
    const {index} = currentBoardDetails(boardId);
    const updatedBoardList = boards.slice(0, index).concat(updatedBoard).concat(boards.slice(index + 1))
    setCurrentBoard(updatedBoard);
    setBoards(updatedBoardList);
  }

  const addTaskToListInContext = task => {
    setTasks([...tasks, task])
  }

  const updateTaskRecordsInContext = () => {
    TaskService.fetchTasksByBoardId(currentBoard._id).then(resp => {
        // console.log('resp: ', resp.data)
        setTasks([...resp.data.tasks])
      })
  }

  const updateTaskRecordPriorityToggle = (taskId, boolean) => {
    setTasks(prevState => {
      return [...prevState].map(task => {
        if(task._id === taskId) {
          task.priority = boolean;
        }
        return task;
      })
    })
  }

  const handleTaskPriorityToggle = async (task, taskId) => {
    // setPriority(!priority)
    let priority = false;
    if(task.hasOwnProperty('priority')) {
      priority = task.priority
    }
    const response = await TaskService.updateTaskPriority({ priority: !priority }, taskId, currentBoard._id)
    // console.log('response: ', response)
    const data = response.data.task;
    // console.log('data: ', data)
    updateTaskRecordPriorityToggle(taskId, data.priority)
  }

  const removeBoardFromContext = boardId => {
    const updatedBoardList = boards.filter(board => board._id !== boardId);
    setBoards([...updatedBoardList])
    removeCurrentBoardProfile();
  }

  const deleteTaskFromContext = () => {
    // instead of removing the deleted task from the array
    // make another network call
    TaskService.fetchTasksByBoardId(currentBoard._id).then(resp => {
        // console.log('resp: ', resp)
        setTasks([...resp.data.tasks])
      })
  }

  const handleSetBoardsToContext = (boards) => {
    setBoards(boards);
    // setBoardListDetailsToContext(boards)
  }

/*  const takeOwnership = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    const userId = user.id;
    BoardService.addOwnership(currentBoard._id, userId)
  }*/

  return (
    <AppContext.Provider
      value={{
        tasks,
        boards,
        currentBoard,
        // currentBoardId,
        setCurrentBoard,
        getBoards,
        createNewBoard,
        updateCurrentBoardProfile,
        removeCurrentBoardProfile,
        removeBoardFromContext,
        addTaskToListInContext,
        updateTaskRecordsInContext,
        // updateTaskRecordPriorityToggle,
        handleTaskPriorityToggle,
        deleteTaskFromContext,
        handleSetBoardsToContext,
        // takeOwnership
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

export const useGlobalContext = () => {
  return useContext(AppContext)
}

export { AppContext, AppProvider };

