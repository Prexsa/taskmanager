import { useState } from 'react';
import { useGlobalContext } from './context';
import BoardService from '../services/board.service';
import TaskService from '../services/task.service';

export default function TaskListPopoverViewModel() {
  const { currentBoardId, updateCurrentBoardProfile, updateTaskRecordsInContext } = useGlobalContext();
  const [visible, setVisible] = useState(false);
  const [popoverContent, setPopoverContent] = useState('');

  const handleDropListActions = actionType => {
    // console.log('actionType: ', actionType)
    switch(actionType) {
      case('add'):
        // toggleAddTaskForm();
        setVisible(false);
        break;
      case('move'):
        setPopoverContent('move');
        break;
      case('transfer'):
        setPopoverContent('transfer');
        break;
      default:
        setVisible(false);
    }
  }

  const handlePopConfirm = async (listId) => {
    const ids = {
      boardId: currentBoardId,
      listId: listId
    }
    const response = await BoardService.deleteListFromBoard(ids);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.board)
    setVisible(false);
  };

  const handleMenuOnClick = async ({key}) => {
    const listIndex = '';
    const response = await BoardService.changeBoardListPosition(listIndex, key, currentBoardId);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.updatedBoard)
    setVisible(false);
  };

  const handleTransfer = async (listId, newListId) => {
    const response = await TaskService.transferAllTasks(listId, newListId);
    console.log('response: ', response)
    updateTaskRecordsInContext(newListId);
    setVisible(false);
  };

  return {
    visible,
    setVisible,
    handleDropListActions,
    popoverContent,
    setPopoverContent,
    handlePopConfirm,
    handleMenuOnClick,
    handleTransfer
  }
}