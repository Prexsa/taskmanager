import { Routes, Route } from "react-router-dom";
import { Layout } from 'antd';
import './App.css';
// import HeaderComponent from './components/Header';
// import ContentComponent from './components/Content';
import ProtectedRoute from './components/ProtectedRoute';
import Login from './components/notLoggedIn/Login';
import ForgotPassword from './components/notLoggedIn/ForgotPassword';
import Register from './components/notLoggedIn/Register';
import CollabRegister from './components/notLoggedIn/CollabRegister';
import Dashboard from './components/Dashboard';

export default function App() {
  return (
    <Layout className="app">
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/collab-register" element={<CollabRegister />} />
        <Route path="/forgot-password" element={<ForgotPassword /> } />
        <Route
          path="/dashboard"
          element={
            <ProtectedRoute>
              <Dashboard />
            </ProtectedRoute>
          }
        />
      </Routes>
    </Layout>
  );
};


// https://exogen.github.io/blog/focus-state/
