const formatDate = dateStr => {
  const d = new Date(dateStr);
  const year = d.getFullYear();
  const month = d.getMonth();
  const day = d.getDate();
  const str = `${month}.${day}.${year}`;
  return str;
}

export default formatDate;