import axios from 'axios';
import authHeader from './auth-header';
import { DOMAIN } from './index.js';
const USER_URI = `${DOMAIN}/api/v1/user`;

const getUserBoard = () => {
  return axios.get(USER_URI + 'user', { headers: authHeader() })
}

const getUserBoards = async userId => {
  return axios.get(USER_URI + `/${userId}`, { headers: authHeader() })
}

const addBoardToUserRecord = (userId, boardId) => {
  // console.log('boardId: ', boardId)
  return axios.patch(`${USER_URI}/${userId}/add-board`, { boardId }, { headers: authHeader() })
}

const removeBoardFromUserRecord = (userId, boardId) => {
  return axios.patch(`${USER_URI}/${userId}/remove-board`, { boardId }, { headers: authHeader() })
}

const getAllUserProfiles = () => {
  return axios.get(USER_URI, { headers: authHeader() });
}

const getUsers = (query) => {
  return axios.post(USER_URI, { query } , { headers: authHeader() });
}


const UserService = {
  getUserBoard,
  getUserBoards,
  addBoardToUserRecord, // updateUserBoardRecord removed, changed to 
  removeBoardFromUserRecord,
  getAllUserProfiles,
  getUsers
}

export default UserService;
