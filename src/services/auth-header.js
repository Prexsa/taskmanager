export default function authHeader() {
  const user = JSON.parse(localStorage.getItem('user'));
  // console.log('user: ', user)
  if(user && user.accessToken) {
    // for Node.js Express back-end
    // console.log('accessToken: ', user)
    return { 'x-access-token': user.accessToken }
  } else {
    return {};
  }
}

// https://www.bezkoder.com/react-hooks-jwt-auth/
// https://www.bezkoder.com/react-express-authentication-jwt/
