import axios from 'axios';
import authHeader from './auth-header';
import { DOMAIN } from './index.js';
const TASK_URI = `${DOMAIN}/api/v1/task`;
// const HEADERS_ACCESS_TOKEN = { headers:  authHeader() }

const fetchTasksByBoardId = async boardId => {
  if(!boardId) return;

  return axios.get(`${TASK_URI}/${boardId}`, { headers:  authHeader() })
}

const addTask = async (values, listId, boardId, userId) => {
  const body = { ...values, listId, boardId, userId };
  return axios.post(`${TASK_URI}`, body, { headers:  authHeader() })
}

const deleteTask = async (taskId) => {
  return axios.delete(`${TASK_URI}/${taskId}`, { headers:  authHeader() })
}

const updateTask = async (values, taskId) => {
  return axios.patch(`${TASK_URI}/${taskId}`, { ...values }, { headers:  authHeader() });
}

const updateTaskPriority = async (values, taskId, boardId) => {
  return axios.patch(`${TASK_URI}/${boardId}/${taskId}`, { ...values }, { headers:  authHeader() });
}

const moveTask = async (listId, taskId) => {
  return axios.patch(`${TASK_URI}/${taskId}`, { listId }, { headers:  authHeader() })
}

const transferAllTasks = async (listId, newListId) => {
  const body = { listId, newListId };
  return axios.patch(`${TASK_URI}`, body, { headers:  authHeader() })
}

const getAllTasks = () => {
  return axios.get(`${TASK_URI}`, { headers:  authHeader() })
}



const TaskService = {
  fetchTasksByBoardId,
  addTask,
  updateTask,
  updateTaskPriority,
  moveTask,
  transferAllTasks,
  deleteTask,
  getAllTasks
}

export default TaskService;
