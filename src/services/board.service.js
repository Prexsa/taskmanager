import axios from 'axios';
import authHeader from './auth-header';
import { DOMAIN } from './index.js';
const BOARD_URI = `${DOMAIN}/api/v1/board`;
let HEADERS_ACCESS_TOKEN = { headers:  authHeader() }
// console.log('HEADERS_ACCESS_TOKEN: ', HEADERS_ACCESS_TOKEN)
const fetchAllBoards = async () => {
  return axios.get(BOARD_URI, HEADERS_ACCESS_TOKEN)
}

const createBoard = values => {
  const user = JSON.parse(localStorage.getItem('user'));
  const userId = user.id;
  return axios.post(`${BOARD_URI}/create`, { userId, ...values }, HEADERS_ACCESS_TOKEN)
}

const deleteBoard = async boardId => {
  const user = JSON.parse(localStorage.getItem('user'));
  const userId = user.id;
  // console.log('userId; ', userId)
  return axios.post(`${BOARD_URI}/${boardId}/delete`, { userId }, HEADERS_ACCESS_TOKEN)
}

const getBoardById = async boardId => {
  HEADERS_ACCESS_TOKEN = { headers: authHeader() };
  return axios.get(`${BOARD_URI}/${boardId}`, HEADERS_ACCESS_TOKEN)
}

const editBoardTitle = async values => {
  // console.log('values: ', values)
  const { title, boardId } = values;
  return axios.patch(`${BOARD_URI}/${boardId}/edit-title`, { title }, HEADERS_ACCESS_TOKEN)
}

const addListToBoard = async values => {
  const { boardId } = values;
  return axios.patch(`${BOARD_URI}/${boardId}/add-boardlist`, { ...values }, HEADERS_ACCESS_TOKEN)
}

const deleteListFromBoard = async ids => {
  const { listId, boardId } = ids;
  return axios.delete(`${BOARD_URI}/${boardId}/${listId}/delete-boardlist`, HEADERS_ACCESS_TOKEN)
}

const changeBoardListPosition = async (currentListIndex, newListIndex, boardId) => {
  if(currentListIndex === newListIndex) return;

  const body = {
    currentListIndex, 
    newListIndex
  }

  return axios.patch(`${BOARD_URI}/${boardId}/change-boardlist-position`, body, HEADERS_ACCESS_TOKEN)
}

const renameBoardList = async values => {
  // console.log('values: ', values);
  const { title, listId, boardId } = values;
  return axios.patch(`${BOARD_URI}/${boardId}/${listId}/edit-boardlist-title`, { title }, HEADERS_ACCESS_TOKEN)
}

const addCollab = async (values) => {
  const { boardId } = values;
  return axios.patch(`${BOARD_URI}/${boardId}/add-collab`, { ...values }, HEADERS_ACCESS_TOKEN)
}

const removeCollab = async (values) => {
  const { boardId, userId, key } = values;
  return axios.patch(`${BOARD_URI}/${boardId}/remove-collab`, { userId, key }, HEADERS_ACCESS_TOKEN)
}

const addOwnership = async (boardId, userId) => {
  return axios.patch(`${BOARD_URI}/${boardId}/add-ownership`, { userId }, HEADERS_ACCESS_TOKEN)
}

const removeOwnership = async (boardId, userId) => {
  return axios.patch(`${BOARD_URI}/${boardId}/remove-ownership`, { userId }, HEADERS_ACCESS_TOKEN)
}

const BoardService = {
  fetchAllBoards,
  createBoard,
  deleteBoard,
  getBoardById,
  editBoardTitle,
  addListToBoard,
  deleteListFromBoard,
  changeBoardListPosition,
  renameBoardList,
  addCollab,
  removeCollab,
  addOwnership,
  removeOwnership
}

export default BoardService;
