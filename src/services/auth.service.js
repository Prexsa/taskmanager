import axios from 'axios';
import { DOMAIN } from './index.js';
const AUTH_URI = `${DOMAIN}/api/v1/auth`;

const checkEmail = email => {
  return axios.post(AUTH_URI + '/check-email', { ...email })
}

const register = async values => {
  return await axios.post(AUTH_URI + '/signup', { ...values })
}

const login = async values => {
  return await axios.post(AUTH_URI + '/signin', { ...values })
  // console.log('response: ', response)
  /*return axios
    .post(AUTH_URI + '/signin', { ...values })
    .then(response => {
      if(response.data.accessToken) {
        localStorage.setItem('user', JSON.stringify(response.data))
      }
      return response.data;
    })*/
}

const logout = () => {
  localStorage.removeItem('user');
}

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem('user'));
}

const updatePassword = values => {
  return axios.post(AUTH_URI + `/forgot-password`, { ...values })
}

const collabRegister = values => {
  return axios.post(AUTH_URI + `/collab-register`, { ...values })
}

const AuthService = {
  checkEmail,
  register,
  login,
  logout,
  getCurrentUser,
  updatePassword,
  collabRegister
}

export default AuthService;