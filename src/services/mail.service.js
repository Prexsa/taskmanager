import axios from 'axios';
import authHeader from './auth-header';
import { DOMAIN } from './index.js';
const NODEMAILER_URI = `${DOMAIN}/api/v1/mail`;

const sendMail = (values) => {
  return axios.post(NODEMAILER_URI, { ...values }, { headers: authHeader() })
}

const MailService = {
  sendMail
}

export default MailService;