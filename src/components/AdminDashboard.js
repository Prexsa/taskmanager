import { useState } from 'react';
// import UserService from '../services/user.service';

const AdminDashboard = () => {
  const [profiles, setProfiles] = useState(null);
  // const [boards, setBoards] = useState([]);

  /*const getAllUsersProfiiles = async () => {
    const response = await UserService.getAllUserProfiles();
    // console.log('response: ', response)
    setProfiles(response.data.users)
    response.data.users.map(user => {
      const ownBoards = user.ownBoards;
      setBoards([...boards, ...ownBoards])
    })
  }*/

  /*useEffect(() => {
    // getAllUsersProfiiles();
  }, [])*/

  // console.log('boards: ', boards)

  if(profiles === null) return;

  return (
    <>
      <h3>Admin Dashboard</h3>
      {
        profiles.map((profile, index) => {
          const email = profile.email;
          const boards = profile.ownBoards;

          return (
            <div key={index} style={{border: '1px solid grey', margin: '5px', padding: '10px'}}>
              <h3>User email: {email}</h3>    
              <ul style={{margin: '10px 0px'}}>
                {
                  boards.map((board, idx) => {
                    const boardId = board.boardId
                    return (
                      <li className="admin-board-list" key={idx} style={{margin: '5px 0px'}}>{boardId}</li>
                    )
                  })
                }
              </ul>
            </div>
          )
        })
      }
    </>
  )
}

export default AdminDashboard;