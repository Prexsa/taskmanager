import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import { Select } from 'antd';
import { useGlobalContext } from '../models/context';

const SelectBoardList = ({ childFunc }) => {
  const { boards, setCurrentBoard, currentBoard } = useGlobalContext();
  const [value, setValue] = useState("Workspaces")
  const handleClear = () => { setValue('Workspaces') }

  const handleSelectBoard = boardName => {
    const workspace = boards.filter(board => board.title === boardName);
    setValue(workspace[0].title)
    setCurrentBoard(workspace[0])
  }

  useEffect(() => {
    childFunc.current = handleClear;
  }, [childFunc])

  useEffect(() => {
    setValue(currentBoard.title)
  }, [currentBoard])

  return (
    <Select
      onChange={handleSelectBoard}
      onClear={handleClear}
      value={value}
      style={{width: 120}}
      bordered={false}
      options={
        boards.map((board, index) => {
          return {
            value: board.title,
            label: board.title
          }
        })
      }
    />
  )
}

SelectBoardList.propTypes = {
  childFunc: PropTypes.object
}

export default SelectBoardList;