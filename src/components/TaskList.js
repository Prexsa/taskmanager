import PropTypes from 'prop-types';
import { useState, useRef, useEffect, useCallback } from 'react';
import { useGlobalContext } from '../models/context';
import TaskCard from './TaskCard';
import AddTaskShowBtn from './AddTask.ShowBtn';
import TaskCardDetails from './TaskCardDetails';
import TaskListHeader from './TaskListHeader';
import AddTaskForm from './form/AddTask.Form';

const TaskList = ({ taskList, list, listIndex, contentRef }) => {
  const { tasks } = useGlobalContext();
  const listRef = useRef();
  const containerRef = useRef();
  const [visible, setVisible] = useState(false);
  const [taskDetails, setTaskDetails] = useState({});
  const [toggleAddTaskForm, setToggleAddTaskForm] = useState(false);
  const [filteredTasks, setFilteredTasks] = useState([]);
  const [overflowScrollY, setOverflowScrollY] = useState({ overflowY: 'hidden', height: 'auto'});
  const tasklistName = taskList.name;
  const listId = taskList._id;

  const showModal = (idx, task) => {
    setVisible(true);
    setTaskDetails({...task, idx})
  }

  const closeModal = () => {
    setVisible(false);
    setTaskDetails({})
  }

  const handleTaskCardUpdate = (taskId) => {
    console.log('handleTaskOnChanges: ', taskId)
    const task = tasks.filter(task => task._id === taskId)[0];
    console.log('task: ', task)
    setTaskDetails(task)
  }

  const handleToggleAddTask = () => setToggleAddTaskForm(true);

  const resizeListHeight = useCallback(() => {
    if(contentRef !== null) {
      const { height } = contentRef
      const currentHeight = listRef.current.clientHeight;
      if(currentHeight > height) {
        const adjustedColumnHeight = height - 200;
        // console.log('adjustedColumnHeight: ', adjustedColumnHeight)
        setOverflowScrollY({ overflowY: 'scroll', height: adjustedColumnHeight + 'px' })
      }
    }
  }, [contentRef])

  const scrollToView = isVisible => {
    const { overflowY, height } = overflowScrollY;
    if(overflowY === 'scroll') {
      if(isVisible) {
        const adjustedColumnHeight = parseInt(height) - 100;
        setOverflowScrollY({ overflowY: 'scroll', height: adjustedColumnHeight + 'px' })
      } else {
        const adjustedColumnHeight = parseInt(height) + 100;
        setOverflowScrollY({ overflowY: 'scroll', height: adjustedColumnHeight + 'px' })
      }
    }
  }

  useEffect(() => {
    resizeListHeight();
  }, [filteredTasks, resizeListHeight])

  useEffect(() => {
    window.addEventListener("resize", resizeListHeight)
  }, [resizeListHeight]);

  useEffect(() => {
    const filterByListId = [...tasks].filter(task => listId === task.listId);
    const filterHasPriority = [...filterByListId].filter(task => task?.priority);
    const filterByNoPriorityKey = [...filterByListId].filter(task => task?.priority === false || task?.priority === undefined);
    const priorityKeyOnTop = [...filterHasPriority, ...filterByNoPriorityKey];
    // console.log('priorityKeyOnTop: ', priorityKeyOnTop)
    setFilteredTasks([...priorityKeyOnTop])
  }, [tasks, taskList, listId])

  const taskCards = filteredTasks.map((task, idx) => {
    return (
      <div key={idx}>
        <TaskCard
          task={task}
          list={list}
          listId={listId}
          taskIndex={idx}
          showModal={showModal}
          closeModal={closeModal}
          // handleMoveTask={handleMoveTask}
        />
      </div>
    )
  })

  return (
    <div className="tasklist-container" >
      <TaskListHeader 
        list={list} 
        listId={listId}
        tasklistName={tasklistName}
        listIndex={listIndex} 
        addTaskToggle={handleToggleAddTask} 
      />
      <div className="tasklist-addtask-top-pos">
        {
          toggleAddTaskForm && 
          <AddTaskForm
            listId={listId}
            // taskList={taskList}
            onCancel={() => setToggleAddTaskForm(false)}
            onBlur={() => setToggleAddTaskForm(false)} 
          />
        }
      </div>
      <div className='tasklist-list' ref={listRef} style={overflowScrollY}>
        { taskCards }
      </div>
      {
        // Modal for taskcard details
        visible &&
        <TaskCardDetails
          task={taskDetails}
          list={list}
          listId={listId}
          listName={tasklistName}
          visible={visible}
          onCancel={closeModal}
          updateTaskCard={handleTaskCardUpdate}
        />
      }
      <div className="tasklist-footer" ref={containerRef}>
        <AddTaskShowBtn listId={listId} scrollToView={scrollToView} />
      </div>
    </div>
  )
}

TaskList.propTypes = {
  taskList: PropTypes.object,
  list: PropTypes.array,
  listIndex: PropTypes.number,
  contentRef: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number
  })
}

export default TaskList;

// https://www.positronx.io/how-to-find-non-fixed-size-html-element-dimension-in-react/
// https://codingbeautydev.com/blog/react-get-element-height-after-render/
// https://www.kindacode.com/article/react-get-the-width-height-of-a-dynamic-element/
