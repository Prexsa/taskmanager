import PropTypes from 'prop-types';
import { useState } from 'react';
import { Modal, Button } from 'antd';
// import { EditOutlined } from '@ant-design/icons';
import BoardTitleForm from './form/BoardTitle.Form';
import DeleteBoard from './DeleteBoard.Popconfirm';

const Board = ({currentBoard}) => {
  // console.log('currentBoard: ', currentBoard)
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Button type="text" onClick={handleOpen} className="title-btn">
        <h2 className="board-title">{currentBoard.title}</h2>
      </Button>
      <Modal title="Board Details" open={open} onCancel={handleClose} footer={null}>
        <div className="board-row">
          <h3 className="title">Board title</h3>
          <div className="board-row-content">
            <p>Edit board name</p>
            <BoardTitleForm title={currentBoard.title} />
          </div>
        </div>
        <div className="board-row">
          <h3 className="title delete-row-title">Delete board</h3>
          <div className="board-row-content">
            <p>This will permanently delete the workspace</p>
            <DeleteBoard boardId={currentBoard._id} />
          </div>
        </div>
      </Modal>
    </>
  )
}

Board.propTypes = {
  currentBoard: PropTypes.shape({
    title: PropTypes.string,
    list: PropTypes.array,
    _id: PropTypes.string
  })
}

export default Board;