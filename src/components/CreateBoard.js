import CreateBoardForm from './form/CreateBoard.Form';

const CreateBoard = () => {
  return (
    <div className="createboard-container">
      <h3>Create your first board</h3>
      <CreateBoardForm />
    </div>
  )
}

export default CreateBoard;