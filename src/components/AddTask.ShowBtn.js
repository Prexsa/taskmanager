import PropTypes from 'prop-types';
import React, { useState } from 'react'
import { Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import AddTaskForm from './form/AddTask.Form';

const AddTaskShowBtn = ({ listId, scrollToView }) => {
  const [showForm, setShowForm] = useState(false);

  const handleOnClick = () => {
    setShowForm(!showForm)
    scrollToView(!showForm);
  }

  if(showForm) {
    return (
      <AddTaskForm
        listId={listId}
        onCancel={handleOnClick}
        onBlur={handleOnClick}
      />
    )
  }

  return (
    <Button 
      className="add-task-btn"
      style={{textAlign: 'left', paddingLeft: '15px'}}
      icon={<PlusOutlined />}
      onClick={handleOnClick}
      block
    >
       Add a card...
    </Button>
  )
}

AddTaskShowBtn.propTypes = {
  listId: PropTypes.string,
  scrollToView: PropTypes.func,
}

export default AddTaskShowBtn;