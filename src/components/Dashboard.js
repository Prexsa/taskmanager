import { useEffect } from 'react';
import { useGlobalContext } from '../models/context';
import FixedTopBar from './FixedTopBar';
import Content from './Content';
import BoardList from './BoardList';

const Dashboard = () => {
  const { currentBoard, getBoards } = useGlobalContext();
  // const userId = localStorage.getItem('userId');
  const user = JSON.parse(localStorage.getItem('user'))
// console.log('user: ', user)
  useEffect(() => {
    getBoards(user.id)
  }, [user.id, getBoards])

  return (
    <div className="dashboard">
    <FixedTopBar currentBoardId={currentBoard._id} />
    {
      currentBoard._id === '' ?
      <BoardList />
      :
      <Content />
    }
    </div>
  )
}

export default Dashboard;