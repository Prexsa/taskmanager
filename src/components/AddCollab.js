import { useState, useMemo, useEffect, useCallback } from 'react';
import { Input, Modal, Tooltip } from 'antd';
import { UserAddOutlined, MinusCircleOutlined, CrownOutlined } from '@ant-design/icons';
import { useGlobalContext } from '../models/context';
import UserService from '../services/user.service';
import MailService from '../services/mail.service';
import BoardService from '../services/board.service';
import debounce from '../utils/debounce';

const AddCollabForm = () => {
  const { currentBoard } = useGlobalContext();
  console.log('currentBoard: ', { currentBoard })
  const user = JSON.parse(localStorage.getItem('user'))
  const userId = user.id;
  const [visible, setVisible] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [options, setOptions] = useState([]);
  const [feedback, setFeedback] = useState({ show: false, msg: ''})
  const [ownerFeedback, setOwnerFeedback] = useState({ show: false, msg: '' })
  const [collabs, setCollabs] = useState([]);
  const [owners, setOwners] = useState([]);
  const boardId = currentBoard._id;

  const handleOnChange = useCallback(async (e) => {
    console.log(e.target.value);
    if(e.target.value === '') {
      setIsOpen(false);
      setOptions([]);
    }
    const response = await UserService.getUsers(e.target.value);
    const options = response.data.users.filter((user) => user._id !== userId);
    // console.log('options: ', options)
    if(options.length > 0 && e.target.value !== '') {
      setIsOpen(true);
      setOptions([...options]);
    } else {
      setIsOpen(false);
      setOptions([]);
    }
  }, [userId]);

  const handleOnPressEnter = async (e) => {
    const value = e.target.value;
    console.log('value: ', value)
    const emailRegex = new RegExp(/^[A-Za-z0-9_!#$%&'*+=?`{|}~^.-]+@[A-Za-z0-9.-]+$/,'gm',);
    const isEmailValid = emailRegex.test(value);
    if(isEmailValid) {
      // console.log('validate')
      // if email matches dont' do anything
      const isEmailFound = await collabs.filter(collab => collab.email === value);
      console.log('isEmailFound: ', isEmailFound)
      if(isEmailFound.length > 0) return;
      const response = await MailService.sendMail({ email: value, boardId });
      const { userId } = response.data;
      // console.log('mail response: ', response)
      const boardResponse = await BoardService.addCollab({ email: value, boardId, userId })
      console.log('response: ', boardResponse)
      setFeedback({ show: true, msg: 'Email sent' });
      return;
    }
    // console.log('not validate')
    setFeedback({show: true, msg: 'Invalid email'});
  };

  const handleOptionSelect = async (option) => {
    // console.log('option: ', option)
    const { _id, email } = option;
    // update collab state
    // update user records
    UserService.addBoardToUserRecord(_id, boardId);
    // console.log('userResponse: ', userResponse)
    BoardService.addCollab({ email, boardId, userId: _id });
    // console.log('boardResponse: ', boardResponse)
    setCollabs([...collabs, option]);
  };

  const handleOnCancel = (e) => {
    setVisible(false);
    setIsOpen(false);
    setOptions([]);
  };

  const handleModalToggle = () => setVisible(true);

  const debounceHandler = useMemo(() => debounce(handleOnChange, 500), [handleOnChange]);

  const handleRemoveUser = async (index, userId, key) => {
    // console.log('remove user!')
    // console.log('keys; ', key)
    if(key === 'owners' && owners.length === 1) {
      setOwnerFeedback({ show: true, msg: 'Board needs to have at least one owner' })
      return;
    }
    // remove user from board collabs
    BoardService.removeCollab({ boardId, userId, key });
    // console.log('remove collab: ', response)
    if(key === 'owners') {
      const filtered = owners.filter((collab, i) => i !== index);
      // console.log('filteredCollabs: ', filteredCollabs)
      setOwners(filtered);
    } else {
      const filtered = collabs.filter((collab, i) => i !== index);
      // console.log('filteredCollabs: ', filteredCollabs)
      setCollabs(filtered);
    }
  }

  const handleMakeUserOwner = async (index, userId, isOwner) => {
    console.log('handleMakeUserOwner ')
    if(isOwner && owners.length === 1) {
      setOwnerFeedback({ show: true, msg: 'Board needs to have at least one owner' })
      return;
    }
    if(isOwner) {
      // remove user from owner's
      const response = await BoardService.removeOwnership(boardId, userId);
      const found = owners.filter((owner, idx) => idx === index);
      const updatedOwnersArray = owners.filter((owner, idx) => idx !== index);
      setOwners(updatedOwnersArray)
      setCollabs([...collabs, ...found])
      console.log('removeOwnership: resp: ', response)
      return;
    }
    const response = await BoardService.addOwnership(boardId, userId)
    const found = collabs.filter((collab, idx) => idx === index);
    const updatedCollabsArray = collabs.filter((collab, idx) => idx !== index)
    setOwners([...owners, ...found])
    setCollabs(updatedCollabsArray)
    console.log('addOwnership: resp: ', response)
  }

  useEffect(() => {
    if(feedback.show === false) return;
    const timer = setTimeout(() => {
        setFeedback({ show: false, msg: '' })
    }, 3000);

    return () => clearTimeout(timer);
  }, [feedback]);

  useEffect(() => {
    if(ownerFeedback.show === false) return;
    const timer = setTimeout(() => {
        setOwnerFeedback({ show: false, msg: '' })
    }, 3000);

    return () => clearTimeout(timer);
  }, [ownerFeedback]);

  useEffect(() => {
    setOwners(currentBoard.owners)
    setCollabs(currentBoard.collab)
  }, [currentBoard.collab, currentBoard.owners])

  return (
    <>
      <div onClick={handleModalToggle} className="add-collab-btn">
        <UserAddOutlined /> Add
      </div>
      <Modal
        title="Add Collaborators"
        transitionName=""
        maskTransitionName=""
        width={730}
        className="modal-container"
        open={visible}
        footer={null}
        onCancel={handleOnCancel}
        destroyOnClose={true}
      >
        <div className="board-row">
          <h3 className="title">Add Contributors</h3>
          <div className="board-row-content">
            <p>Search by name or email, or send an email request</p>
            <Input
              allowClear
              // value={inputValue}
              // className="add-collab-input"
              placeholder="email address or name"
              onChange={debounceHandler}
              onPressEnter={handleOnPressEnter}
            />
            <div className={`${feedback.show ? 'show' : 'hide'}`}>
              <p>{feedback.msg}</p>
            </div>
            <ul className={`addcollab-options ${isOpen ? 'show' : ' '}`}>
              {options.length > 0 && options.map((option, index) => (
                <li
                  key={index}
                  className="addcollab-option"
                  onClick={(e) => {
                    e.stopPropagation();
                    handleOptionSelect(option);
                    setIsOpen(false);
                  }}
                >
                  {`${option.firstname} ${option.lastname} < ${option.email} >`}
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="board-row">
          <h3 className="title">Active Contributors</h3>
          <div className="board-row-content">
            <div className="add-collab-board-title-wrapper">
              <h4>Board Owners</h4>
              {
                ownerFeedback.show ? <span className="add-collab-feedback-color">{ownerFeedback.msg}</span> : null
              }
            </div>
            <ul>
            {
              owners.map((owner, i) => {
                // console.log('collab: ', owner)
                const fullName = `${owner.firstname} ${owner.lastname}`;
                const userId = owner._id;
                const email = owner.email;
                const pending = owner.pending;

                const backgroundColor = `${pending ? 'orange' : 'transparent'}`

                return (
                  <li key={i} className="add-collab-user-list" style={{ backgroundColor: backgroundColor }}>
                    <Tooltip title="Remove ownership">
                      <CrownOutlined className="crown-icon" onClick={(isOwner) => handleMakeUserOwner(i, userId, isOwner = true)} />
                    </Tooltip>
                    <Tooltip title="Remove">
                      <MinusCircleOutlined className="minus-icon" onClick={(key) => handleRemoveUser(i, userId, key = "owners")} />
                    </Tooltip>
                    <div className="add-collab-list-item">
                      {fullName}&nbsp; &lt; {email} &gt;
                    </div>
                  </li>
                )
              })
            }
            </ul>
            <h4>Collaborators</h4>
            <ul>
            {
              collabs.length <= 0 ? 
              <div>There are no collaborators</div>
              :
              collabs.map((collab, i) => {
                // console.log('collab: ', collab)
                const fullName = `${collab.firstname} ${collab.lastname}`;
                const userId = collab._id;
                const email = collab.email;
                const pending = collab.pending;

                const backgroundColor = `${pending ? 'orange' : 'transparent'}`

                return (
                  <li key={i} className="add-collab-user-list" style={{ backgroundColor: backgroundColor }}>
                    <Tooltip title="Take ownership">
                      <CrownOutlined className="crown-icon" onClick={(isOwner) => handleMakeUserOwner(i, userId, isOwner = false)} />
                    </Tooltip>
                    <Tooltip title="Remove">
                      <MinusCircleOutlined className="minus-icon" onClick={(key) => handleRemoveUser(i, userId, key = "collab")} />
                    </Tooltip>
                    <div className="add-collab-list-item">
                      {fullName}&nbsp; &lt; {email} &gt;
                    </div>
                  </li>
                )
              })
            }
            </ul>
          </div>
        </div>
      </Modal>
    </>
  );
}

export default AddCollabForm;