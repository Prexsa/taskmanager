import PropTypes from 'prop-types';
import { useGlobalContext } from '../models/context';

const BoardList = () => {
  const { boards, setCurrentBoard, currentBoard } = useGlobalContext();
  
  if(currentBoard._id !== '') return;

  return (
    <div className="boardlist-container">
      <h3>Your Workspaces</h3>
      <ul className="boardlist-ul">
      {
        boards.map((board, i) => {
          return (
            <li
              key={i} 
              onClick={() => setCurrentBoard(board)}
            >
              {board.title}
            </li>
            )
        })
      }
      </ul>
    </div>
  )
}

BoardList.propTypes = {
  currentBoard: PropTypes.string,
}

export default BoardList;