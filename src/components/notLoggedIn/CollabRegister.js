import './notLoggedIn.css';
import { useEffect } from 'react';
import useViewModel from './register.viewModel';
import { useSearchParams } from "react-router-dom";
import { Button, Form, Input } from 'antd';

export default function Register() {
  const [form] = Form.useForm();
  const { collabRegister, setCollabCredentials } = useViewModel();
  const [queryParameters] = useSearchParams();
  
  useEffect(() => {
    const email = queryParameters.get('email');
    const boardId = queryParameters.get('boardId');
    const userId = queryParameters.get('userId');
    if(typeof email === "object") return; // if typeof is "object" params doesn't exist
    if(typeof email === 'string') {
      form.setFieldValue("email", email)
      setCollabCredentials({ boardId, userId });
    }
  }, [queryParameters, setCollabCredentials, form])

  return (
    <div className="register-container">
      <div className="register-header">
        <h3>Sign up for an account</h3>
      </div>
        <Form
          layout='vertical'
          form={form}
          onFinish={collabRegister}
        >
          <Form.Item name="email">
            <Input 
              className="form-input-email"
              disabled
            />
          </Form.Item>
          <Form.Item 
            name="password"
            rules={[
              {
                required: true,
                message: 'Create a password'
              }
            ]}
          >
            <Input.Password placeholder="Create a password" />
          </Form.Item>
          <Form.Item 
            name="firstname"
            rules={[
              {
                required: true,
                message: 'First name is required'
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  // console.log('value: ', value)
                  const hasNumber = /\d/;
                  if(hasNumber.test(value)) {
                    return Promise.reject(new Error('First name should not contain numerical value'))
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input placeholder="First name" />
          </Form.Item>
          <Form.Item 
            name="lastname"
            rules={[
              {
                required: true,
                message: 'Last name is required'
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  // console.log('value: ', value)
                  const hasNumber = /\d/;
                  if(hasNumber.test(value)) {
                    return Promise.reject(new Error('Last name should not contain numerical value'))
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input placeholder="Last name" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Register</Button>
          </Form.Item>
        </Form>
    </div>
  )
}
