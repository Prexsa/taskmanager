import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import AuthService from '../../services/auth.service';
import BoardService from '../../services/board.service';

export default function RegisterViewModel() {
  const navigate = useNavigate();
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [emailFeedback, setEmailFeedback] = useState({ status:  "", message: "" });
  const [email, setEmail] = useState(null);
  const [collabCredentials, setCollabCredentials] = useState({ boardId: '', userId: '' })

  const checkEmail = (values) => {
    AuthService.checkEmail(values)
      .then((response) => {
        console.log('response: ', response)
        const { isValid } = response.data;
        if(isValid) {
          setEmailFeedback({ status: 'error', message: "This email address is already in use." });
          return;
        }
        setEmail({...values});
        setEmailFeedback({ status: '', message: "" })
        setIsEmailValid(false);
    })
  }

  const register = (values) => {
    AuthService.register(values)
    .then((response) => {
        // setMessage(response.data.message);
        // setSuccessful(true)
        console.log('response: ', response)
        localStorage.setItem('user', JSON.stringify(response.data))
        navigate(`/dashboard`, { replace: true });
      },
      (error) => {
        /*const resMessage =
          (error.response && error.response.data && error.response.data.message) ||
          error.message || error.toString();*/
        // setMessage(resMessage);
        // setSuccessful(false);
      }
    )
  }

  const collabRegister = (values) => {
    const { boardId, userId } = collabCredentials;
    const data = { ...values, boardId, userId }
    AuthService.collabRegister(data)
      .then((response) => {
        // setMessage(response.data.message);
        // setSuccessful(true)
        console.log('response: ', response)
        localStorage.setItem('user', JSON.stringify(response.data))
        navigate(`/dashboard`, { replace: true });
      },
      (error) => {
        /*const resMessage =
          (error.response && error.response.data && error.response.data.message) ||
          error.message || error.toString();*/
        // setMessage(resMessage);
        // setSuccessful(false);
      }
    )
  }

  const toggleEditEmail = () => setIsEmailValid(true);

  const clearErrorState = (chVal, allVal) => {
    setEmailFeedback({ status: '', message: '' })
  }

  const getUserEmail = (boardId) => {
    BoardService.getBoardById(boardId)
      .then(response => {
        console.log('response ', response)
      })
  }

  /*const onFinishFailed = ({values, errorFields}) => {
    // console.log('values: ', values, ' errorFields: ', errorFields)
    errorFields.forEach(field => {
      if(field.name[0] === 'email') {
        setEmailError({
          status: 'error',
          message: field.errors[0]
        })
      }
      if(field.name[0] === 'password') {
        setPasswordError({
          status: 'error',
          message: field.errors[0]
        })
      }
    })
  }*/

  return {
    email,
    isEmailValid,
    emailFeedback,
    checkEmail,
    register,
    collabRegister,
    toggleEditEmail,
    clearErrorState,
    getUserEmail,
    setCollabCredentials,
    // onFinishFailed
  }
}