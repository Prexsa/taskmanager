import './notLoggedIn.css';
import useViewModel from './register.viewModel';
import { Link } from "react-router-dom";
import { Button, Form, Input } from 'antd';
import { EditOutlined } from '@ant-design/icons';

export default function Register() {
  const [form] = Form.useForm();
  const {
    email,
    isEmailValid,
    emailFeedback,
    checkEmail,
    register,
    toggleEditEmail,
    clearErrorState,
  } = useViewModel();

  return (
    <div className="register-container">
      <div className="register-header">
        <h3>Sign up for an account</h3>
      </div>
      {
        isEmailValid ?
        <Form
          layout='vertical'
          form={form}
          onFinish={checkEmail}
          // onFinishFailed={onFinishFailed}
          onValuesChange={clearErrorState}
        > 
          <Form.Item 
            name="email"
            validateStatus={emailFeedback.status}
            help={emailFeedback.message}
            rules={[
              {
                required: true,
                message: 'Enter an email address'
              }
            ]}
          >
            <Input placeholder="Enter email address" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Check email</Button>
          </Form.Item>
        </Form>
        :
        <Form
          layout='vertical'
          form={form}
          onFinish={register}
        >
          <Form.Item name="email">
            <Input 
              className="form-input-email"
              addonAfter={<EditOutlined onClick={toggleEditEmail} />} 
              disabled
              value={email.email}
            />
          </Form.Item>
          <Form.Item 
            name="password"
            rules={[
              {
                required: true,
                message: 'Create a password'
              }
            ]}
          >
            <Input.Password placeholder="Create a password" />
          </Form.Item>
          <Form.Item 
            name="firstname"
            rules={[
              {
                required: true,
                message: 'First name is required'
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  // console.log('value: ', value)
                  const hasNumber = /\d/;
                  if(hasNumber.test(value)) {
                    return Promise.reject(new Error('First name should not contain numerical value'))
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input placeholder="First name" />
          </Form.Item>
          <Form.Item 
            name="lastname"
            rules={[
              {
                required: true,
                message: 'Last name is required'
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  // console.log('value: ', value)
                  const hasNumber = /\d/;
                  if(hasNumber.test(value)) {
                    return Promise.reject(new Error('Last name should not contain numerical value'))
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input placeholder="Last name" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Register</Button>
          </Form.Item>
        </Form>
      }
      <div className="register-footer">
        <Link className="register-footer-link" to='/'>Already have an account? Log in</Link>
      </div>
    </div>
  )
}
