import './notLoggedIn.css';
import useViewModel from './login.viewModel';
import { Link } from "react-router-dom";
import { Button, Form, Input } from 'antd';

export default function Login() {
  const [form] = Form.useForm();
  const {
    signin,
    clearErrorState,
    emailError,
    passwordError,
    onFinishFailed
  } = useViewModel();

  return (
    <div className="login-container">
      <div className="login-header">
        <h3>Log in to continue to:</h3>
        <h3>Task Manager</h3>
      </div>
        <Form
          layout='vertical'
          form={form}
          onFinish={signin}
          onFinishFailed={onFinishFailed}
          onValuesChange={clearErrorState}
        >
          <Form.Item 
            name="email"
            validateStatus={emailError.status}
            help={emailError.message}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Enter an email address '
              },
            ]}
          >
            <Input placeholder="Enter email" />
          </Form.Item>
          <Form.Item 
            name="password"
            validateStatus={passwordError.status}
            help={passwordError.message}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Enter a password'
              },
            ]}
          >
            <Input.Password placeholder="Enter password" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Continue</Button>
          </Form.Item>
        </Form>
        <div className="login-footer">
          <ul>
            <li>
              <Link to='/forgot-password'>Forgot password?</Link>
            </li>
            <li>
              <p className="dot">&#8226;</p>
            </li>
            <li>
              <Link to="/register">Sign up for an account</Link>
            </li>
          </ul>
        </div>
    </div>
  )
}