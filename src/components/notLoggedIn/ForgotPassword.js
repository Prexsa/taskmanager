import './notLoggedIn.css';
import useViewModel from './forgotPassword.viewModel';
import { Link } from 'react-router-dom';
import { Button, Form, Input,  } from 'antd';
import { EditOutlined } from '@ant-design/icons';

export default function ForgotPassword() {
  const [form] = Form.useForm();
  const {
    email,
    isEmailValid,
    emailFeedback,
    checkEmail,
    updatePassword,
    toggleEditEmail,
    clearErrorState,
  } = useViewModel();

  return (
    <div className="forgot-container">
      <div className="forgot-header">
        <h3>Reset Password</h3>
        <h3>Enter a new password</h3>
      </div>
      {
        isEmailValid ?
        <Form
          layout='vertical'
          form={form}
          onFinish={checkEmail}
          onValuesChange={clearErrorState}
        >
          <Form.Item
            name="email"
            validateStatus={emailFeedback.status}
            help={emailFeedback.message}
            rules={[
              {
                required: true,
                message: 'Please input your email!',
              }
            ]}
            hasFeedback
          >
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Check email</Button>
          </Form.Item>
        </Form>
        :
        <Form
          layout='vertical'
          form={form}
          onFinish={updatePassword}
        >
          <Form.Item name="email">
            <Input 
              className="form-input-email"
              addonAfter={<EditOutlined onClick={toggleEditEmail} />} 
              disabled value={email.email} 
            />
          </Form.Item>
          <Form.Item
            name="password"
            label="Password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Please confirm your password!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject(new Error('The two passwords that you entered do not match!'));
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" block>Submit</Button>
          </Form.Item>
        </Form>
      }
      <div className="forgot-footer">
        <Link className="forgot-footer-link" to="/">back to login</Link>
      </div>
    </div>
  )
}