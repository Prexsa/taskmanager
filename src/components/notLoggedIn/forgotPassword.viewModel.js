import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import AuthService from '../../services/auth.service';

export default function ForgotPasswordViewModel() {
  const navigate = useNavigate();
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [emailFeedback, setEmailFeedback] = useState({ status:  "", message: "" });
  const [email, setEmail] = useState(null);

  const checkEmail = values => {
    // console.log('checkEmail: ', values)
    AuthService.checkEmail(values)
      .then((response) => {
        // console.log('response: ', response.data)
        const { isValid } = response.data;
        if(isValid) {
          // const { userId } = response.data;
          setEmail({...values});
          // setUserId(userId)
          setEmailFeedback({ status: '', message: "Found" })
          setIsEmailValid(false);
          return;
        }
        setEmailFeedback({ status: 'error', message: "Email does not exist in our records" });
      }
    )
  }

  const updatePassword = values => {
    // console.log('values: ', values)
    AuthService.updatePassword({ ...values, ...email })
      .then(() => {
        navigate('/', {replace: true });
        // window.location.reload();
      },
      (error) => {
        const resMessage = 
          (error.response && error.response.data && error.response.data.message) ||
          error.message || error.toString();
          console.log('resMessage: ', resMessage)
      }
    )
  }

  const toggleEditEmail = () => setIsEmailValid(true);

  const clearErrorState = (chVal, allVal) => {
    setEmailFeedback({ status: '', message: ''})
  }

  return {
    isEmailValid,
    email,
    emailFeedback,
    checkEmail,
    updatePassword,
    toggleEditEmail,
    clearErrorState,
  }
}