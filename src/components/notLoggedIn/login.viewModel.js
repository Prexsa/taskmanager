import { useState } from 'react';
import { useNavigate } from "react-router-dom";
import AuthService from '../../services/auth.service';

export default function LoginViewModel() {
  const navigate = useNavigate();
  const [emailError, setEmailError] = useState({ status: '', message: '' })
  const [passwordError, setPasswordError] = useState({ status: '', message: '' })

  const signin = (values) => {
    // console.log('values: ', values)
    return AuthService.login(values).then(
      (response) => {
        const { isValid, message, itemName } = response.data;
        if(isValid) {
          // user is authenticated
          localStorage.setItem('user', JSON.stringify(response.data))
          navigate(`/dashboard`);
          /*navigate(`/dashboard`, { replace: true });
          window.location.reload();*/
          return { message }
        } else {
          if(itemName === 'email') {
            setEmailError({
              status: 'error',
              message: message
            })
          }
          if(itemName === 'password') {
            setPasswordError({
              status: 'error',
              message: message
            })
          }
        }
      },
      (error) => {
        console.log('auth login: ', error)
        /*const resMessage =
          (error.response && error.response.data && error.response.data.message) ||
          error.message || error.toString();
        // setLoading(false);
        setMessage(resMessage);*/
      }
    )
  }

  const clearErrorState = (chVal, allVal) => {
    const key = Object.keys(chVal);
    if(key[0] === 'email') {
      setEmailError({ status: '', message: '' })
    }
    if(key[0] === 'password') {
      setPasswordError({ status: '', message: '' })
    }
  }


  const onFinishFailed = ({values, errorFields}) => {
    // console.log('values: ', values, ' errorFields: ', errorFields)
    errorFields.forEach(field => {
      if(field.name[0] === 'email') {
        setEmailError({
          status: 'error',
          message: field.errors[0]
        })
      }
      if(field.name[0] === 'password') {
        setPasswordError({
          status: 'error',
          message: field.errors[0]
        })
      }
    })
  }

  return {
    signin,
    emailError,
    passwordError,
    clearErrorState,
    onFinishFailed
  }
}
