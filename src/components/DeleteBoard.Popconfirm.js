import PropTypes from 'prop-types';
import { Popconfirm, Button } from 'antd';
import { useGlobalContext } from '../models/context';
import BoardService from '../services/board.service';

const DeleteBoard = ({boardId}) => {
  // console.log('boardId: ', boardId)
  const { removeBoardFromContext } = useGlobalContext();
  const handlePopConfirm = async () => {
    await BoardService.deleteBoard(boardId)
    removeBoardFromContext(boardId);
  }

  return (
    <Popconfirm
      placement="bottomRight"
      title="Are you sure to delete this board?"
      onConfirm={handlePopConfirm}
      // onCancel={handlePopCancel}
      okText="Yes"
      cancelText="No"
    >
      <Button type="primary" danger size="small">
      Delete
      </Button>
    </Popconfirm>
  )
}

DeleteBoard.propTypes = {
  boardId: PropTypes.string
}

export default DeleteBoard;