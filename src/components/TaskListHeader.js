import PropTypes from 'prop-types';
import ListTitleForm from './form/ListTitle.Form';
import TaskListPopover from './Popover/TaskList.Popover';

export default function TaskListHeader({list, listId, tasklistName, listIndex, addTaskToggle}) {
  return (
    <div className="tasklist-header-container">
      <ListTitleForm 
        listId={listId}
        tasklistName={tasklistName}
      />
      <TaskListPopover 
        list={list} 
        listId={listId}
        listIndex={listIndex}
        toggleAddTaskForm={addTaskToggle}
      />
    </div>
  )
}


TaskListHeader.propTypes = {
  list: PropTypes.array,
  listId: PropTypes.string,
  tasklistName: PropTypes.string,
  listIndex: PropTypes.number,
  addTaskToggle: PropTypes.func
}