import PropTypes from 'prop-types';
import { Card, Popconfirm } from 'antd';
import { EditOutlined, DeleteOutlined, ArrowRightOutlined, ExclamationOutlined } from '@ant-design/icons';
import { useGlobalContext } from '../models/context';
import TaskService from '../services/task.service';
import TaskCardDetailsMoveActionsPopover from './Popover/TaskCardDetailsMoveActions.Popover';

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "Septemeber",
  "October",
  "November",
  "December"
]

const TaskCard = ({task, list, listId, taskIndex, showModal}) => {
  const { deleteTaskFromContext, handleTaskPriorityToggle } = useGlobalContext();
  // console.log('task: ', task)
  const title = task.title;
  const taskId = task._id;
  const createDate = task.date;

  const formatDateUTC = createDate => {
    // format UI date
    const date = new Date(createDate);
    const day = date.getDate();
    const month = date.getMonth();
    const dateStr = `${months[month].slice(0,3)} ${day}`;
    return dateStr;
  }

  const handlePopConfirm = async (taskId) => {
    await TaskService.deleteTask(taskId);
    // console.log('response ', response)
    deleteTaskFromContext();
  }

  return (
    <Card className={`taskcard-container ${task?.priority ? 'priority-active-border' : ''}`}>
      <div className={`taskcard-priority-wrapper`}>
        <div className={`${task?.priority ? 'priority-active' : 'priority-hidden'}`}>
          {/*<span className="taskcard-priority-indicator"></span>*/}
          Priority
        </div>
        <ExclamationOutlined
          className="taskcard-icon exclamation-outlined"
          onClick={() => handleTaskPriorityToggle(task, taskId)}
        />
      </div>
      <div 
        className="taskcard-body" 
        onClick={() => showModal(taskIndex, task)}
      >
        <div className="taskcard-title">{title}</div>
        <EditOutlined className="taskcard-edit-icon" key="edit" />
      </div>
      <div className="taskcard-footer">
        <p>
          {formatDateUTC(createDate)}
        </p>
        <div>
          <Popconfirm
            title="Delete this task?"
            onConfirm={() => handlePopConfirm(taskId, taskIndex)}
            // onCancel={handlePopCancel}
            okText="Yes"
            cancelText="No"
          >
            <DeleteOutlined 
              className="taskcard-icon delete-icon"
              key="delete" 
            />
          </Popconfirm>
          <TaskCardDetailsMoveActionsPopover 
            taskId={taskId} 
            list={list} 
            listId={listId} 
          >
            <ArrowRightOutlined 
              className="taskcard-icon right-arrow-icon"
              key='move'
            />
          </TaskCardDetailsMoveActionsPopover>
        </div>
      </div>
    </Card>
  )
}

TaskCard.propTypes = {
  task: PropTypes.object,
  list: PropTypes.array,
  listId: PropTypes.string,
  taskIndex: PropTypes.number,
  showModal: PropTypes.func
}

export default TaskCard;