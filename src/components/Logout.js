// import { Link } from "react-router-dom";
// import { Button } from 'antd';
import AuthService from '../services/auth.service';

const Logout = () => {
  const handleLogout = () => {
    AuthService.logout();
  }

  return <a href="/" onClick={handleLogout}>Logout</a>
}

export default Logout;