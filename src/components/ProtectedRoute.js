import { Navigate } from "react-router-dom";

/*const parseJwt = token => {
  try {
    return JSON.parse(atob(token.split(".")[1]));
  } catch(err) {
    return null;
  }
}*/

export default function ProtectedRoute({children}) {
  const user = JSON.parse(localStorage.getItem('user'));
  const accessToken = user.accessToken;
  // console.log('user: ', accessToken)
  return accessToken !== '' ? children : <Navigate to='/' />
}