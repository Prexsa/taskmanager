import PropTypes from 'prop-types';
import { useRef } from 'react';
import { useGlobalContext } from '../models/context';
import AccountPopover from './Popover/Account.popover';
import CreateBoardModal from './CreateBoard.Modal';
import SelectBoardList from './SelectBoardList';

const FixedTopBar = ({ currentBoardId }) => {
  const { removeCurrentBoardProfile } = useGlobalContext();
  const childFunc = useRef(null);

  const handleClearSelect = () => {
    removeCurrentBoardProfile()
    childFunc.current()
  }

  return (
    <div className="fixedTB">
      <div className="fixedTB-left">
        <h1 onClick={handleClearSelect}>Task Manager</h1>
        <div className="fixedTB-popover-list">
        {
          currentBoardId !== ''
          ?
          <>
            <CreateBoardModal />
            <SelectBoardList childFunc={childFunc} />
          </>
          : null
        }
        </div>
      </div>
      <div className="fixedTB-right">
        <AccountPopover />
      </div>
    </div>
  )
}

FixedTopBar.propTypes = {
  currentBoardId: PropTypes.string
}

export default FixedTopBar;