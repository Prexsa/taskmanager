import { useState, useCallback } from 'react';
import { Layout, Row, Col } from 'antd';
import { useGlobalContext } from '../models/context';
import BoardModal from './Board.Modal';
import AddCollab from './AddCollab';
import TaskList from './TaskList';
import AddListForm from './form/AddList.Form';

const { Content } = Layout;

const useClientRect = () => {
  const [rect, setRect] = useState(null);
  const ref = useCallback(node => {
    // console.log('node: ', node.getBoundingClientRect())
    if(node !== null) {
      setRect(node.getBoundingClientRect())
    }
  }, [])
  return [rect, ref];
}

const DisplayBoard = () => {
  const { currentBoard } = useGlobalContext();
  const [rect, ref] = useClientRect();

  if(currentBoard.title === "" || currentBoard._id === "") return null;
  const { list, _id } = currentBoard;

  if(list === undefined) return null;

  return (
    <Content ref={ref} className="content">
      <div className="content-header">
        <BoardModal currentBoard={currentBoard} />
        <AddCollab />
      </div>
      <section className="content-section">
        <Row
          gutter={16}
          wrap={true}
          className='row-wrapper'
          style={{
            overflowX: 'scroll',
            overflowY: 'hidden',
            // border: '1px solid blue',
            height: `calc(100% - 80px)`
          }}
          >
          {
            list.map((listItem, idx) => {
              return (
                <Col key={idx} span={3} className="content-column">
                  <TaskList
                    listIndex={idx}
                    taskList={listItem}
                    // boardId={_id}
                    list={list}
                    contentRef={rect}
                  />
                </Col>
              )
            })
          }
          <Col span={3} className="content-column">
            <div className="content-col-add-container">
              <AddListForm index={_id} boardId={_id} />
            </div>
          </Col>
        </Row>
      </section>
    </Content>
  )
}

export default DisplayBoard;
// https://www.manuelkruisz.com/blog/posts/react-width-height-resize-hook