import PropTypes from 'prop-types';
import { Modal, Popconfirm, Space } from 'antd';
import { ArrowRightOutlined, MinusOutlined, ExclamationOutlined } from '@ant-design/icons';
import { useGlobalContext } from '../models/context';
import TaskService from '../services/task.service';
import formatDate from '../utils/formatDate';
import TaskCardDetailsTitleForm from './form/TaskCardDetailsTitle.Form';
import TaskCardDetailsDescriptionForm from './form/TaskCardDetailsDescription.Form';
import TaskCardDetailsMoveActionsPopover from './Popover/TaskCardDetailsMoveActions.Popover';

const TaskCardDetails = ({ task, list, listId, listName, visible, onCancel, updateTaskCard }) => {
  const { updateTaskRecordsInContext, handleTaskPriorityToggle } = useGlobalContext();
  const taskId = task._id;
  const taskIndex = task.idx;
  const taskTitle = task.title;
  const taskDate = task.date;
  
  // const listName = list.filter(listItem => listItem._id === task.listId)[0].label;
  /* 
    transitionName="" & maskTransitionName="" to remove react warning
  */
  const handleOnConfirm = async () => {
    const response = await TaskService.deleteTask(taskId);
    console.log('response: ', response)
    updateTaskRecordsInContext();
    onCancel();
  }

  const handleTaskCardUpdate = () => {
    handleTaskPriorityToggle(task, taskId)
    updateTaskCard(taskId)
  }

  return (
    <Modal
      transitionName=""
      maskTransitionName="" 
      width={730}
      className="modal-container"
      open={visible}
      footer={null}
      onCancel={onCancel}
      >
        <div className="modal-header">
          <div className="modal-title">
            {task?.priority ? <p className="priority-active">Priority</p> : <p></p>}
            <TaskCardDetailsTitleForm title={taskTitle} taskId={taskId} taskIndex={taskIndex} />
            <p>in list <span>{listName}</span></p>
          </div>
        </div>

        <div className="modal-body">
          <div className="left">
            <section>
              <h3>Description</h3>
              <TaskCardDetailsDescriptionForm task={task} taskId={taskId} taskIndex={taskIndex} />
            </section>
            <section>
              <h3>Activity</h3>
              <div className="modal-section-list">
                <p><span>Prexsa</span> added this card</p>
                <p>{formatDate(taskDate)}</p>
              </div>
            </section>
          </div>
          <div className="right">
            <h4>Actions</h4>
            <ul className="taskcard-detail-actions-list">
              <li onClick={handleTaskCardUpdate}>
                <Space>
                  <ExclamationOutlined />
                  Make Priority
                </Space>
              </li>
              <li>
                <TaskCardDetailsMoveActionsPopover taskId={taskId} list={list} listId={listId} onCancel={onCancel}>
                  <Space>
                    <ArrowRightOutlined />
                    Move To
                  </Space>
                </TaskCardDetailsMoveActionsPopover>
              </li>
              <li name="delete">
                <Popconfirm
                  placement="bottomRight"
                  title="Are you sure to delete this task card?"
                  onConfirm={handleOnConfirm}
                  // onCancel={handlePopCancel}
                  okText="Yes"
                  cancelText="No"
                >
                  <Space>
                    <MinusOutlined />
                    Delete
                  </Space>
                </Popconfirm>
              </li>
            </ul>
          </div>
        </div>

      </Modal>
  )
}

TaskCardDetails.propTypes = {
  task: PropTypes.object,
  list: PropTypes.array,
  listId: PropTypes.string,
  listName: PropTypes.string,
  visible: PropTypes.bool,
  onCancel: PropTypes.func,
  updateTaskCard: PropTypes.func
}

export default TaskCardDetails;