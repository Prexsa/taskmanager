import BoardViewModal from '../models/Board';

const BoardListDropdown = () => {
  const { boards, handleDropdownSelect } = BoardViewModal();

  return (
    <div className="boardlistdropdown-container">
      <h2>Workspaces</h2>
      <ul className="boardlistdropdown-ul">
      {
        boards.map((board, index) => {
          // console.log('board: ', board._id)
          const boardId = board._id;
          return (
            <li
              type="primary" 
              key={index}
              className="boardlistdropdown-li"
            >
              <div 
                className=".boardlistdropdown-tile" 
                onClick={() => handleDropdownSelect(boardId)}
              >
                  <span>{board.title}</span>
              </div>
            </li>
          )
        })
      }
      </ul>
    </div>
  )
}

export default BoardListDropdown;