import { useState, useEffect } from 'react';
import BoardService from '../services/board.service';
import UserService from '../services/user.service';

import { Radio, Space, Select } from 'antd';
const { Option } = Select;

export default function AddBoardToUser() {
  const [boards, setBoards] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    BoardService.fetchAllBoards().then((response) => {
        // console.log('response: ', response)
        setBoards([...response.data.boards]);
    });

    UserService.getAllUserProfiles().then((response) => {
        // console.log('response: ', response)
        setUsers([...response.data.users]);
    });
  }, []);

  const handleOnChange = async (values) => {
    // console.log('value: ', values)
    const parsed = JSON.parse(values);
    const { userId, boardId } = parsed;
    UserService.addBoardToUserRecord(userId, boardId).then((response) => {
        // console.log('response: ', response)
        setUsers([...users, response.data.user]);
    });
  };

  return (
    <>
      <div>
      <h3>User Records</h3>
      <ul style={{ margin: '5px', padding: '5px' }}>
        {users.map((user, i) => {
          return (
            <div key={i} style={{ margin: '5px', padding: '5px' }}>
              <h3>Email: {user.email}</h3>
              <div>
                <h4>Boards Assigned</h4>
                {user.ownBoards.map((board, j) => {
                  return <p key={j}>Board id: {board.boardID}</p>;
                })}
              </div>
            </div>
          );
        })}
      </ul>
    </div>
    <ul>
      {boards.map((board, i) => {
        return (
          <div
            key={i}
            style={{
              border: '1px solid grey',
              margin: '5px',
              padding: '5px',
            }}
          >
            <div>
              <p>{board._id}</p>
              <p>{board.title}</p>
              <h4>Users</h4>
              <Select
                defaultValue="User"
                style={{width: 200}}
                onChange={handleOnChange}
              >
                {users.map((user, i) => {
                  return (
                    <Option
                      key={i}
                      value={JSON.stringify({
                        userId: user._id,
                        boardId: board._id,
                      })}
                    >
                      {user.email}
                    </Option>
                  );
                })}
              </Select>
            </div>
          </div>
              );
          })
      }
      </ul>
    </>
  );
}