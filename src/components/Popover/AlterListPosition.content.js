import './sharedPopover.css';
import PropTypes from 'prop-types';
import { useGlobalContext } from '../../models/context';
import BoardService from '../../services/board.service';
import { Dropdown, Menu } from 'antd';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';

export default function AlterListPosition({list, listIndex, handleClosePopover, handleBack}) {
  const { currentBoardId, updateCurrentBoardProfile } = useGlobalContext();

  const handleMenuOnClick = async ({key}) => {
    const response = await BoardService.changeBoardListPosition(listIndex, key, currentBoardId);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.updatedBoard)
    handleClosePopover();
  };

  const menu = (
      <Menu
        onClick={handleMenuOnClick}
        items={
          list.map((listItem, position) => {
            return {
              key: position,
              label: (<div>{position === listIndex ? `${position + 1} (current)` : position + 1}</div>),
            }
          })
        }
      />
    );

    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="icons" onClick={handleBack} />
          <h2>Move list...</h2>
          <CloseOutlined className="icons" onClick={handleClosePopover} />
        </div>
        <div className="popover-content">
          <h4>Position</h4>
          <Dropdown 
            overlay={menu}
            trigger={['click']}
            onClick={(e) => e.preventDefault()}
            className="tasklist-popover-dropdown-overlay"
          >
            <div>
              {listIndex + 1}
            </div>
          </Dropdown>
        </div>
      </div>
    )
}


AlterListPosition.propTypes = {
  list: PropTypes.array,
  listIndex: PropTypes.string,
  handleClosePopover: PropTypes.func,
  handleBack: PropTypes.func
}