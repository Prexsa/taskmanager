import './sharedPopover.css';
import PropTypes from 'prop-types';
import { useState } from 'react';
import { useGlobalContext } from '../../models/context';
import BoardService from '../../services/board.service';
import { Popover, Popconfirm } from 'antd';
import { MoreOutlined, LeftOutlined, CloseOutlined } from '@ant-design/icons';
import AlterListPosition from './AlterListPosition.content';
import TransferAllTask from './TransferAllTask.content';

export default function TaskListPopover({list, listId, listIndex, toggleAddTaskForm}) {
  const { currentBoardId, updateCurrentBoardProfile } = useGlobalContext();
  const [visible, setVisible] = useState(false);
  const [popoverContent, setPopoverContent] = useState('');

  const handleBack = () => setPopoverContent();
  const handleClosePopover = () => {
    setVisible(false)
    handleBack()
  };

  const handlePopConfirm = async (listId) => {
    const ids = {
      boardId: currentBoardId,
      listId: listId
    }
    const response = await BoardService.deleteListFromBoard(ids);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.board)
    handleClosePopover();
  };

  const handleDropListActions = actionType => {
    // console.log('actionType: ', actionType)
    switch(actionType) {
      case('add'):
        toggleAddTaskForm();
        handleClosePopover();
        break;
      case('move'):
        setPopoverContent('move');
        break;
      case('transfer'):
        setPopoverContent('transfer');
        break;
      default:
        handleClosePopover()
    }
  }

  const ListActionsSelect = (listId) => {
    const actions = [
      {
        name: 'add card...',
        action: 'add'
      },
      {
        name: 'change list position...',
        action: 'move'
      },
      {
        name: 'transfer all cards to...',
        action: 'transfer'
      },
    ]

    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="icons icon-hidden" />
          <h2>List Actions</h2>
          <CloseOutlined className="icons" onClick={handleClosePopover} />
        </div>
        <div className="popover-content">
          <ul>
            {
              actions.map((action, index) => {
                return (
                  <li
                    key={index}
                    onClick={() => handleDropListActions(action.action)}
                  >
                  {action.name}
                  </li>
                )
              })
            }
            <li className="popover-last-child-delete">
              <Popconfirm
                title="Are you sure to delete?"
                onConfirm={() => handlePopConfirm(listId)}
                // onCancel={handlePopCancel}
                okText="Yes"
                cancelText="No"
              >
                <div>delete</div>
              </Popconfirm>
            </li>
          </ul>
        </div>
      </div>
    )
  }

  const renderContent = () => {
    switch(popoverContent) {
      case('move'):
        return (
          <AlterListPosition 
            list={list}
            listIndex={listIndex}
            handleClosePopover={handleClosePopover}
            handleBack={handleBack}
          />
        )
        // break;
      case('transfer'):
        return (
          <TransferAllTask 
            list={list}
            listId={listId}
            handleClosePopover={handleClosePopover}
            handleBack={handleBack}
          />
          )
        // break;
      default:
        return (
          <ListActionsSelect
            listId={listId}
            handleClosePopover={handleClosePopover}
          />
          )
    }
  }

  return (
    <Popover
      content={renderContent()}
      trigger="click"
      placement="bottom"
      open={visible}
      onOpenChange={() => setVisible(!visible)}
    >
      <MoreOutlined className="icon-more" />
    </Popover>
  )
}


TaskListPopover.propTypes = {
  list: PropTypes.array,
  listId: PropTypes.string,
  listIndex: PropTypes.number,
  toggleAddTaskForm: PropTypes.func
}