import { useState } from 'react';
import { Popover, Space } from 'antd';
import { LeftOutlined, PlusOutlined, CloseOutlined, DownOutlined } from '@ant-design/icons';
import CreateBoardForm from '../form/CreateBoard.Form';

export default function CreateBoardPopover() {
  const [visible, setVisible] = useState(false);

  const handleSetVisible = () => setVisible(!visible);

  const content = () => {
    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="popover-icon-invisible"/>
          <h2>Create a board</h2>
          <CloseOutlined className="icons" onClick={() => setVisible(false)} />
        </div>
        <div className="popover-content">
          <CreateBoardForm handleSetVisible={handleSetVisible} />
        </div>
      </div>
    )
  }

  return (
    <Popover
      content={content}
      placement="bottomLeft"
      trigger="click"
      open={visible}
      onOpenChange={() => setVisible(!visible)}
    >
      <Space className="popover-btn">
        Create
        <DownOutlined className="popover-btn-icon" />
      </Space>
    </Popover>
  )
}