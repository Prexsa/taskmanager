import './sharedPopover.css';
import PropTypes from 'prop-types';
import { useGlobalContext } from '../../models/context';
import TaskService from '../../services/task.service';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';

export default function TransferAllTask({list, listId, handleClosePopover, handleBack}) {
  const { updateTaskRecordsInContext } = useGlobalContext();

  const handleTransfer = async (newListId) => {
    await TaskService.transferAllTasks(listId, newListId);
    // console.log('response: ', response)
    updateTaskRecordsInContext(newListId);
    handleClosePopover();
  };

  return (
    <div className="popover-container">
      <div className="popover-header">
        <LeftOutlined className="icons" onClick={handleBack} />
        <h2>Transfer list...</h2>
        <CloseOutlined className="icons" onClick={handleClosePopover} />
      </div>
      <div className="popover-content">
        <ul>
        {
          list.filter((listItem) => listItem._id !== listId).map((listItem, position) => {
            console.log('listItem: ', listItem)
              return <li key={position} onClick={() => handleTransfer(listItem._id)}>{listItem.name}</li>
            })
        }
        </ul>
      </div>
    </div>
  )
}

TransferAllTask.propTypes = {
  list: PropTypes.array,
  listId: PropTypes.string,
  handleClosePopover: PropTypes.func,
  handleBack: PropTypes.func
}