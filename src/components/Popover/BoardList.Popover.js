import './sharedPopover.css';
import { useState } from 'react';

import { useGlobalContext } from '../../models/context';
import { Popover, Space } from 'antd';
import { CloseOutlined, DownOutlined, LeftOutlined } from '@ant-design/icons';

const BoardListPopover = () => {
  const { boards, currentBoardId, setCurrentBoard } = useGlobalContext();
  const [visible, setVisible] = useState(false);
// console.log('boards: ', boards)
  if(Object.keys(boards).length <= 0) return;

  const handleSelectBoard = board => {
    setCurrentBoard(board)
    setVisible(false);
  }

  const content = boards => {
    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="icons popover-icon-invisible" />
          <h2>Workspaces</h2>
          <CloseOutlined className="icons" onClick={() => setVisible(false)} />
        </div>
        <div className="popover-content">
          <ul>
            {
              boards.filter((board, index) => currentBoardId !== index).map((board, index) => {
                return (
                  <li key={index} onClick={() => handleSelectBoard(board)}>
                    {board.title}
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    )
  }

  return (
    <Popover
      content={() => content(boards)}
      placement="bottomLeft"
      trigger="click"
      open={visible}
      onOpenChange={() => setVisible(!visible)}
    >
      <Space className="popover-btn">
        Workspaces
        <DownOutlined className="popover-btn-icon" />
      </Space>
    </Popover>
  )
}

export default BoardListPopover;