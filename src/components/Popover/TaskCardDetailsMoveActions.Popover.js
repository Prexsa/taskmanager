import PropTypes from 'prop-types';
import { useState } from 'react';
import { useGlobalContext } from '../../models/context';
import TaskService from '../../services/task.service';
import { Popover } from 'antd';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';

function TaskCardDetailsMoveActionsPopover({ taskId, list, listId, onCancel, children }) {
  const { updateTaskRecordsInContext } = useGlobalContext();
  const [visible, setVisible] = useState(false);

  const handleMoveTaskTo = async listId => {
    await TaskService.moveTask(listId, taskId);
    // console.log('response: ', response)
    setVisible(false);
    updateTaskRecordsInContext();
    // onCancel();
  }

  const content = () => {
    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="icons icon-hidden" />
          <h2>Move card...</h2>
          <CloseOutlined className="icons" onClick={() => setVisible(false)} />
        </div>
        <div className="popover-content">
          <ul>
          {
            list
              .filter((listItem) => listItem._id !== listId)
              .map((listItem, index) => {
                return (
                  <li key={index} onClick={() => handleMoveTaskTo(listItem._id)}>
                    {listItem.name}
                  </li>
                )
              })
          }
          </ul>
        </div>
      </div>
    )
  }

  return (
    <Popover
      content={content}
      trigger="click"
      placement="bottom"
      open={visible}
      onOpenChange={() => setVisible(!visible)}
    >
      {children}
    </Popover>
  )
}

TaskCardDetailsMoveActionsPopover.propTypes = {
  taskId: PropTypes.string,
  list: PropTypes.array,
  listId: PropTypes.string,
  onCancel: PropTypes.func,
  children: PropTypes.node
}

export default TaskCardDetailsMoveActionsPopover