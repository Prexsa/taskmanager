import './sharedPopover.css';
import { useGlobalContext } from '../../models/context';
// import viewModel from '../../models/tasklist.popover.viewModel';
import BoardService from '../../services/board.service';
import { Popconfirm } from 'antd';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';

export default function ListActions({listId, toggleAddTaskForm, handleClosePopover, setPopoverContent}) {
  const { currentBoardId, updateCurrentBoardProfile } = useGlobalContext();
  const handlePopConfirm = async (listId) => {
    const ids = {
      boardId: currentBoardId,
      listId: listId
    }
    const response = await BoardService.deleteListFromBoard(ids);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.board)
    handleClosePopover();
  };

  const handleDropListActions = actionType => {
    // console.log('actionType: ', actionType)
    switch(actionType) {
      case('add'):
        // toggleAddTaskForm();
        handleClosePopover();
        break;
      case('move'):
        setPopoverContent('move');
        break;
      case('transfer'):
        setPopoverContent('transfer');
        break;
      default:
        handleClosePopover();
    }
  }

  const actions = [
      /*{
        name: 'add card...',
        action: 'add'
      },*/
      {
        name: 'change list position...',
        action: 'move'
      },
      {
        name: 'transfer all cards to...',
        action: 'transfer'
      },
    ]

    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="icons" className="icon-hidden" />
          <h2>List Actions</h2>
          <CloseOutlined className="icons" onClick={handleClosePopover} />
        </div>
        <div className="popover-content">
          <ul>
            <li onClick={toggleAddTaskForm}>
              add card
            </li>
          {
            actions.map((action, index) => {
              return <li key={index} onClick={() => handleDropListActions(action.action)}>{action.name}</li>
            })
          }
            <li className="popover-last-child-delete">
              <Popconfirm
                title="Are you sure to delete?"
                onConfirm={() => handlePopConfirm(listId)}
                // onCancel={handlePopCancel}
                okText="Yes"
                cancelText="No"
              >
                <div>delete</div>
              </Popconfirm>
            </li>
          </ul>
        </div>
      </div>
    )
}