import './sharedPopover.css';
import { useState } from 'react';
import { Popover } from 'antd';
import { 
  CloseOutlined,
  LeftOutlined, 
  UserOutlined } from '@ant-design/icons';
import Logout from '../Logout';

const UserPopover = () => {
  const [visible, setVisible] = useState(false);

  const user = JSON.parse(localStorage.getItem('user'));
  const email = user.email;
  const name = `${user.fname} ${user.lname}`;

  const content = () => {
    return (
      <div className="popover-container">
        <div className="popover-header">
          <LeftOutlined className="popover-icon-invisible"/>
          <h2>Account</h2>
          <CloseOutlined className="icons" onClick={() => setVisible(!visible)} />
        </div>
        <div className="popover-content">
          <div className="member-content">
            <div>{name}</div>
            <div>{email}</div>
          </div>
          <ul>
            <li>
              <Logout />
            </li>
          </ul>
        </div>
      </div>
    )
  }

  return (
    <Popover
      content={content}
      placement="bottomLeft"
      trigger="click"
      open={visible}
      onOpenChange={() => setVisible(!visible)}
    >
      <UserOutlined className="user-outlined-icon icon" style={{color: '#6a6b60'}}/>
    </Popover>
  )
}

export default UserPopover;