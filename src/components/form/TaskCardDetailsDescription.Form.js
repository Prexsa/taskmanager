import PropTypes from 'prop-types';
import {useState, useEffect, useRef} from 'react';
import { useGlobalContext } from '../../models/context';
import TaskService from '../../services/task.service';
import { Form, Input, Button } from 'antd';
const {TextArea} = Input;

const TaskCardDetailsDescription = ({ task, taskId, taskIndex }) => {
  const { currentBoard, updateTaskRecordsInContext } = useGlobalContext();
  const inputRef = useRef(null);
  const [form] = Form.useForm();
  const [inputToggle, setInputToggle] = useState(false);
  // console.log('form: ', form)
  useEffect(() => {
    // reset the form fields so initialValues updates new props values
    form.resetFields();
  }, [form])


  const onFinish = async values => {
    // console.log('value: ', values)
    await TaskService.updateTask(values, taskId);
    updateTaskRecordsInContext(currentBoard._id)
    setInputToggle(false)
  }

  const handleOnFocus = () => {
    inputRef.current.focus({
      cursor: 'all',
    })
    setInputToggle(true)
  }

  return (
    <Form form={form} layout={"vertical"} onFinish={onFinish}>
      <Form.Item name="description">
        <div>
          <TextArea
            defaultValue={task.description}
            className="form-textarea"
            ref={inputRef}
            placeholder={"Add a more detailed description..."}
            autoSize={{ minRows: 3, maxRows: 5 }}
            onFocus={() => handleOnFocus()}
          />
        </div>
      </Form.Item>
      {
        inputToggle ? 
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
          <Button type="text" 
            onClick={() => setInputToggle(false)}
          >
            Cancel
          </Button>
        </Form.Item>
        :
        null
      }
    </Form>
  )
}

TaskCardDetailsDescription.propTypes = {
  task: PropTypes.object.isRequired,
  taskId: PropTypes.string.isRequired,
  taskIndex: PropTypes.number
}

export default TaskCardDetailsDescription;