import PropTypes from 'prop-types';
import { useGlobalContext } from '../../models/context';
import TaskService from '../../services/task.service';
import { Button, Form, Input } from 'antd';
import { CloseOutlined } from '@ant-design/icons';

function AddTaskForm({ listId, onCancel, onBlur }) {
  const { currentBoard, addTaskToListInContext } = useGlobalContext();
  const user = JSON.parse(localStorage.getItem('user'));
  const { id } = user;
  // console.log('user: ', { user, id })
  const [form] = Form.useForm();
// console.log('listId: ', listId)
  const onFinish = async values => {
    // console.log('onFinish: ', values)
    form.resetFields();
    const response = await TaskService.addTask(values, listId, currentBoard._id, id);
    // console.log('response; ', response.data)
    addTaskToListInContext(response.data.task)
  }
  
  return (
      <Form 
        form={form}
        layout={"vertical"}
        onFinish={onFinish}
      >
        <Form.Item 
          name="title"
          // onFocus={onFocus}
          // onBlur={onBlur}
          rules={[
            {
              required: true,
              message: 'Enter a title for this card...'
            }
          ]}
        >
          <Input.TextArea placeholder={"Add a title"} autoFocus />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Add Card
          </Button>
          <CloseOutlined onClick={onCancel} className="add-task-close-icon" />
        </Form.Item>
      </Form>
  )
}

AddTaskForm.propTypes= {
  listId: PropTypes.string,
  onCancel: PropTypes.func,
  onBlur: PropTypes.func
}

export default AddTaskForm;