import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useGlobalContext } from '../../models/context';
import BoardService from '../../services/board.service';

function AddListForm({boardId, index}) {
  const { setCurrentBoard } = useGlobalContext();
  const [form] = Form.useForm();
  
  const onFinish = async values => {
    // console.log('values: ', values)
    if(values.title === undefined) return;
    const titleTrim = values.title.trim();
    values = { title: titleTrim, boardId, index };
    const response = await BoardService.addListToBoard(values);
    // console.log('response: ', response.data.board)
    form.resetFields();
    setCurrentBoard(response.data.board)
  }

  return (
    <Form form={form} layout="inline" onFinish={onFinish}>
      <Form.Item className="add-list-form-item" name="title">
        <Input prefix={<PlusOutlined />} placeholder="Add a list..." />
      </Form.Item>
      <Form.Item>
        <Button 
          className="add-list-form-item-btn" 
          type="primary" 
          htmlType="submit"
        >
          Add
        </Button>
      </Form.Item>
    </Form>
  )
}

AddListForm.propTypes = {
  boardId: PropTypes.string,
  index: PropTypes.string
}

export default AddListForm;