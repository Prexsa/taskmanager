import { useState } from 'react';
import PropTypes from 'prop-types';
import { useGlobalContext } from '../../models/context';
import { Form, Input, Button } from 'antd';

function CreateBoard({handleSetVisible}) {
  const { createNewBoard } = useGlobalContext();
  const [form] = Form.useForm();

  const onFinish = async values => {
    // console.log('onFinish: ', values)
    if(values.title === undefined) return;
    const titleTrim = values.title.trim();
    createNewBoard(titleTrim)
    form.resetFields();
    handleSetVisible();
  }

  return (
    <Form 
      form={form}
      layout="vertical"
      onFinish={onFinish}
    >
      <Form.Item
        label="Board title"
        name="title"
        validateStatus={validateStatus.status}
        help={validateStatus.message}
      >
        <Input />
      </Form.Item>
      <Form.Item shouldUpdate>
        <Button size="small" type="primary" htmlType="submit" block>
          Add
        </Button>
      </Form.Item>
    </Form>
  )
}

CreateBoard.propTypes = {
  handleSetVisible: PropTypes.func
}

export default CreateBoard;