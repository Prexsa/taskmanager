import PropTypes from 'prop-types';
import {useEffect, useRef} from 'react';
import { useGlobalContext } from '../../models/context';
import BoardService from '../../services/board.service';
import { Form, Input } from 'antd';

function BoardTitleForm({title}) {
  const { currentBoard, updateCurrentBoardProfile } = useGlobalContext();
  const inputRef = useRef(null);
  const [form] = Form.useForm();
  // console.log('form: ', form)
  useEffect(() => {
    // reset the form fields so initialValues updates new props values
    form.resetFields();
  }, [title, form])

  const onFinish = async values => {
    // console.log('values: ', values)
    values = {...values, boardId: currentBoard._id };
    // console.log('values: ', values)
    const response = await BoardService.editBoardTitle(values);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.board)
    // handleOnBlur();
  }

  /*const handleOnFocus = () => {
    inputRef.current.focus({
      cursor: 'all',
    })
  }*/

  /*const handleOnBlur = () => {
    // const inputVal = inputRef.current.input.value;
  }*/

  return (
    <Form form={form} onFinish={onFinish} initialValues={{title: title}}>
      <Form.Item name='title' className="form-item">
        <Input
          className="title-input board-title-input"
          ref={inputRef}
          // onFocus={() => handleOnFocus()}
          // onBlur={() => handleOnBlur()}
        />
      </Form.Item>
    </Form>
  )
}

BoardTitleForm.propTypes = {
  title: PropTypes.string
}

export default BoardTitleForm;
// https://dev.to/trishathecookie/react-creating-a-reusable-form-using-react-context-5eof