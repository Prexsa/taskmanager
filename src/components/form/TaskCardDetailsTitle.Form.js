import PropTypes from 'prop-types';
import {useEffect, useRef} from 'react';
import { useGlobalContext } from '../../models/context';
import TaskService from '../../services/task.service';
import { Form, Input } from 'antd';
const {TextArea} = Input;

const TaskCardDetailsTitleForm = ({ title, taskId, taskIndex }) => {
  const { currentBoard, updateTaskRecordsInContext } = useGlobalContext();
  const inputRef = useRef(null);
  const [form] = Form.useForm();
  // const [inputToggle, setInputToggle] = useState(false);
  // console.log('form: ', form)
  useEffect(() => {
    // reset the form fields so initialValues updates new props values
    form.resetFields();
  }, [title, form])

  const onFinish = async values => {
    // console.log('values: ', values)
    await TaskService.updateTask(values, taskId);
    updateTaskRecordsInContext(currentBoard._id)
    handleOnBlur();
  }

  const handleOnPressEnter = e => {
    // to prevent textarea default behavior of newline    
    e.preventDefault();
    form.submit();
  }

  const handleOnFocus = () => {
    inputRef.current.focus({
      cursor: 'all',
    })
    // setInputToggle(true)
  }

  const handleOnBlur = () => {
    // const inputVal = inputRef.current.input.value;
    // setInputToggle(false)
  }

  return (
    <Form form={form} onFinish={onFinish} initialValues={{title: title}}>
      <Form.Item name='title' className="form-item">
        <TextArea
          className="title-input taskcard-details-title-input"
          ref={inputRef}
          onFocus={() => handleOnFocus()}
          onBlur={() => handleOnBlur()}
          autoSize={{
            minRows: 1,
            maxRows: 3,
          }}
          onPressEnter={handleOnPressEnter}
        />
      </Form.Item>
    </Form>
  )
}

TaskCardDetailsTitleForm.propTypes = {
  title: PropTypes.string,
  taskId: PropTypes.string,
  taskIndex: PropTypes.number
}

export default TaskCardDetailsTitleForm;