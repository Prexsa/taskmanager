import PropTypes from 'prop-types';
import { useEffect, useRef } from 'react';
import { useGlobalContext } from '../../models/context';
import BoardService from '../../services/board.service';
import { Form, Input } from 'antd';
const { TextArea } = Input;

export default function ListTitleForm({listId, tasklistName}) {
  const { currentBoard, updateCurrentBoardProfile } = useGlobalContext();
  const inputRef = useRef(null);
  const [form] = Form.useForm();
  // const [inputToggle, setInputToggle] = useState(false);
// console.log('form: ', form)
  useEffect(() => {
    form.resetFields()
  }, [tasklistName, form])

  const onFinish = async values => {
    // console.log('values: ', values)
    values = {...values, listId, boardId: currentBoard._id };
    const response = await BoardService.renameBoardList(values);
    // console.log('response: ', response)
    updateCurrentBoardProfile(response.data.board)
    // handleOnBlur();
  }

  const handleOnPressEnter = e => {
    // to prevent textarea default behavior of newline    
    e.preventDefault();
    form.submit();
  }

  const handleOnFocus = () => {
    inputRef.current.focus({
      cursor: 'all',
    })
    // setInputToggle(true)
  }

  const handleOnBlur = () => {
    // setInputToggle(false)
  }

  return (
    <Form form={form} onFinish={onFinish} initialValues={{title: tasklistName}}>
      <Form.Item name='title' style={{margin: 0}}>
        <TextArea
          className="title-input list-title-input"
          ref={inputRef}
          onFocus={() => handleOnFocus()}
          onBlur={() => handleOnBlur()}
          autoSize={{
            minRows: 1,
            maxRows: 6,
          }}
          onPressEnter={handleOnPressEnter}
        />
      </Form.Item>
    </Form>
  )
}


ListTitleForm.propTypes = {
  listId: PropTypes.string,
  tasklistName: PropTypes.string
}