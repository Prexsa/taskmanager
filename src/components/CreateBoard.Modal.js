import { useState } from 'react';
import { Form, Input, Button, Modal } from 'antd';
import { useGlobalContext } from '../models/context';

const CreateBoardModal = () => {
  const { createNewBoard } = useGlobalContext();
  const [form] = Form.useForm();
  const [open, setOpen] = useState(false);
  const [validateStatus, setValidateStatus] = useState({ status: '', message: ""})

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onFinish = async values => {
    // console.log('onFinish: ', values)
    if(values.title === undefined) {
      setValidateStatus({
        status: 'error',
        message: "Title can't be empty"
      })
      return;
    }
    const titleTrim = values.title.trim();
    createNewBoard(titleTrim)
    form.resetFields();
    handleClose();
  }

  return (
    <>
      <Button type="text" onClick={handleOpen}>
        Create
      </Button>
      <Modal title="Create a board" open={open} onOk={form.submit} onCancel={handleClose}>
        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
        >
          <Form.Item
            label="Board title"
            name="title"
            validateStatus={validateStatus.status}
            help={validateStatus.message}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
}

export default CreateBoardModal;