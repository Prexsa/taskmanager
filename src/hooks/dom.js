
const useRefDimensions = ref => {
  const [dimensions, setDimensions] = useState({ width: 1, height: 1})
  useEffect(() => {
    if(ref.current) {
      const { current } = ref;
      const boundingRect = current.getBoundingClientRect();
      const { width, height } = boundingRect;
      setDimensions({ width: Math.round(width), height: Math.round(height) })
    }
  }, [ref])
  return dimensions;
}

const useClientRect = () => {
  const [rect, setRect] = useState(null);
  const ref = useCallback(node => {
    // console.log('node: ', node.getBoundingClientRect())
    if(node !== null) {
      setRect(node.getBoundingClientRect())
    }
  }, [])
  return [rect, ref];
}

module.exports = {
  useRefDimensions,
  useClientRect
}