const Task = require('../models/task.model');
const { getUserDetails } = require('./user.controller');

exports.getAllTasks = async (req, res) => {
  const tasks = await Task.find({});
  console.log('task: ', tasks)
  res.status(200).json({ tasks })
}

exports.getAllTasksByBoardId = async (req, res) => {
  // console.log('getAllTasksByBoardId: ', req.params)
  if(!req.params) {
    res.send('Params Empty!')
  }
  const { boardId } = req.params;
  const tasks = await Task.find({ boardId: boardId })

  if(!tasks) {
    console.log('no task')
  }
  res.status(200).json({ tasks })
}

exports.createTask = (req, res) => {
  console.log('req : ', req.body)
  const task = new Task({
    title: req.body.title,
    listId: req.body.listId,
    boardId: req.body.boardId,
    userId: req.body.userId
  });
// console.log('task: ', task)
  task.save( async (err, task) => {
    if(err) {
      res.status(500).send({ message: err })
      return;
    }
    res.status(201).json({ task })
  })
  // console.log('task: ', task)
  // res.status(201).json({success: 'hell yeah'})
}


/*exports.getAllTasksByListId = async (req, res) => {
  // console.log('getAllTasksByBoardId: ', req.params)
  const { id } = req.params;
  const tasks = await Task.find({ listID: id })
  
  if(!tasks) {
    console.log('no task')
  }
  res.status(200).json({ tasks })
}*/

/*const getAllTasksByBoardID = (req, res) => {
  console.log('getTask: ', req.params)
  const {id: taskId} = req.params;
  const task = Task.findOne({ id: taskId });


  res.status(200).json({ task })
}*/

exports.addFieldToTask = async (req, res) => {
  const { taskId, boardId } = req.params;
  const task = await Task.findOneAndUpdate(
    { _id: taskId },
    [{ $addFields: { priority: req.body.priority } }],
    { returnDocument: 'after' }
  );

  if(!task) {
    console.log('no task')
  }

  res.status(200).json({ task })
}

exports.updateTask = async (req, res) => {
  // console.log('updateTask: ', req)
  const { taskId } = req.params;
  const task = await Task.findOneAndUpdate(
    { _id: taskId }, 
    { $set: req.body },
    { returnDocument: 'after' }
  );

  if(!task) {
    console.log('no task')
  }

  res.status(200).json({ task })
}

exports.updateManyTasksToNewListId = async (req, res) => {
  const { listId, newListId } = req.body;
  // console.log('body: ', req.body)

  const task = await Task.updateMany(
    { listId: listId },
    { $set: { listId: newListId } }
  );

  if(!task) {
    console.log('no task')
  }

  res.status(200).json({ task })

}

exports.deleteTask = async (req, res) => {
  // console.log('deleteTask: ', req.params)
  const { taskId } = req.params;
  const task = await Task.findOneAndDelete({ _id: taskId });

  if(!task) {
    console.log('No task id')
    return;
  }

  res.status(200).json({ task })
}


/*exports.editDbModelKey = async (req, res) => {
  // 625e67f089267dcf2019a587
  const { taskId } = req.params;
  const task = await Task.updateMany({}, { $rename: { "listID": "listId"}})
  console.log('task: ', task)
  res.status(200).json({ task })
}*/
