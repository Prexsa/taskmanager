const User = require('../models/user.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const JWT_SECRET = process.env.JWT_SECRET;

exports.removeBoardFromUserRecord = async (req, res) => {
  const { userId } = req.params;
  const { boardID } = req.body;
 // console.log('userId: ', userId, ' boardID: ', boardID) 
  const user = await User.findOneAndUpdate({ _id: userId }, { $pull: { ownBoards: { boardId: boardId }}}, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  });

  res.status(201).json({ user })
}

exports.addBoardToUserRecord = async (req, res) => {
  const { userId } = req.params;
  const { boardId } = req.body;
  // return;
  const user = await User.findOneAndUpdate({ _id: userId }, { $push: { ownBoards: { boardId: boardId }}}, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  })
  res.status(201).json({ user });
}

exports.getUserBoards = async (req, res) => {
  const { userId } = req.params;

  const user = await User.find({ _id: userId }, { ownBoards: 1 });
  console.log('User: ', user[0])
  res.status(200).json({ boards: user[0] })
}

exports.getAllUserProfile = async (req, res) => {
  const users = await User.find({});
  res.status(200).json({ users: users})
}

exports.getUsers = async (req, res) => {
  const { query } = req.body;
  const users = await User.find({ $or: [
    {email: {$regex: query , $options: 'i'} },
    {firstname: {$regex: query , $options: 'i'} },
    {lastname: {$regex: query , $options: 'i'} }
    ]
  }, {email: 1, firstname: 1, lastname: 1})
  // const users = await User.find({ email: {$regex: query , $options: 'i'} })
  console.log('users: ', users)
  res.status(200).json({ users })
}


exports.getUserDetails = async (userId) => {
  return await User.find({ _id: userId }, { firstname: 1, lastname: 1 });
}