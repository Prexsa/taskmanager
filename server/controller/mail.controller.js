const nodemailer = require("nodemailer");
const mongoose = require('mongoose');
const mongo = mongoose.mongo;

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.NODEMAILER_AUTH_EMAIL,
    pass: process.env.NODEMAILER_AUTH_PASSWORD
  }
});

transporter.verify((err) => {
  if(err) {
    console.log('nodemailer verify error: ', err)
  } else {
    console.log('Ready to send');
  }
});


exports.sendMail = (req, res) => {
  const mongoId = new mongo.ObjectId();
  const { email, boardId } = req.body;
  /*const email = req.body.email;
  const boardId = req.body.boardId;*/
  const name = "Prexsa";
  const mail = {
    from: 'Task Manager',
    to: 'preksamam@gmail.com',
    subject: 'Collaborate on a project',
    html: `<p>Name: ${name}</p><p>Email: ${email}</p><p>Message: You have been asked to collab on a project.</p>`
    + '<br />'
    + `<a href='http://localhost:3000/collab-register?boardId=${boardId}&userId=${mongoId}&email=${email}'>Click this to redirect to website</a>`,
  };

  transporter.sendMail(mail, (err) => {
    if(err) {
      res.json({ status: "Error", message: err });
      return;
    }
    res.json({ status: "Message Sent", userId: mongoId })
    return;
  });
}

exports.notifyRemoval = (req, res) => {
  const mail = {
    from: 'Task Manager',
    to: 'preksamam@gmail.com',
    subject: 'You have been removed from Task Manager',
    html: `<p>You have been removed from Task Manager.</p>`
  };

  transporter.sendMail(mail, (err) => {
    if(err) {
      res.json({ status: "Error", message: err });
      return;
    }
    res.json({ status: "Message Sent" })
    return;
  });
}