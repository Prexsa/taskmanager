const User = require('../models/user.model');
const Board = require('../models/board.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const JWT_SECRET = process.env.JWT_SECRET;

exports.checkEmail = (req, res) => {
  User.findOne({
    email: req.body.email
  }).exec((err, user) => {
    if(err) {
      res.status(500).send({ message: err });
      return;
    }
    if(user) {
      res.status(200).send({ 
        isValid: true,
        userId: user._id
      });
      return;
    }
// console.log('user: ', user)
    res.status(200).send({ 
      isValid: false
    })
  })
}

exports.signup = (req, res) => {
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    firstname: req.body.firstname,
    lastname: req.body.lastname,
  });
  user.save((err, user) => {
    if(err) {
      res.status(500).send({ message: err });
      return;
    }
    const token = jwt.sign({ id: user._id }, JWT_SECRET, {
        expiresIn: 86400 // 24 hrs
      });
    res.send({ 
      message: 'User was registered successfully!',
      id: user._id,
      email: user.email,
      fname: user.firstname,
      lname: user.lastname,
      accessToken: token,
    });
  })
};

exports.signin = (req, res) => {
  // console.log('req: signin ', req.body)
  User.
    findOne({
      email: req.body.email
    }).exec((err, user) => {
      // console.log('user: ', user)
      if(err) {
        res.status(500).send({ message: err });
        return;
      }
      if(!user) {
        // console.log('null user')
        return res.status(200).send({
          isValid: false,
          message: "Email does not exist in our records",
          itemName: 'email'
        });
      }
      const passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );
      // console.log('passwordIsValid: ', passwordIsValid)
      if(!passwordIsValid) {
        return res.status(200).send({
          isValid: false,
          message: 'Invalid password!',
          itemName: 'password',
        });
      }
      const token = jwt.sign({ id: user._id }, JWT_SECRET, {
        expiresIn: 86400 // 24 hrs
      });
      // console.log('users: ', user)
      res.status(200).send({
        isValid: true,
        id: user._id,
        email: user.email,
        fname: user.firstname,
        lname: user.lastname,
        accessToken: token,
      });
    });
}


exports.updatePassword = async (req, res) => {
  // const { userId } = req.params;
  console.log('userId : ', req.body)
  const {email, password} = req.body;
  const user = await User.
    findOneAndUpdate(
      { email: email },
      { $set: { password: bcrypt.hashSync(password, 8) } }
    )

    res.status(200).send({
      message: 'User password successfully updated!',
      saved: true
    });
}

exports.collabRegister = async (req, res) => {
  const { boardId, userId, email } = req.body;
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    ownBoards: [{ boardId: boardId, collab: true }]
  });
  user.save(async (err, user) => {
    if(err) {
      res.status(500).send({ message: err });
      return;
    }
    const board = await Board.updateOne(
      { _id: boardId, 'collab.email': email },
      { $set: { 'collab.$.userId' : user._id, 'collab.$.pending': false }}
      );

    const token = jwt.sign({ id: user._id }, JWT_SECRET, {
        expiresIn: 86400 // 24 hrs
      });
    res.send({ 
      message: 'User was registered successfully!',
      id: user._id,
      email: user.email,
      fname: user.firstname,
      lname: user.lastname,
      accessToken: token,
    });
  })
}