const Board = require('../models/board.model');
const User = require('../models/user.model');
const TaskList = require('../models/tasklist.model');
const MailControl = require('./mail.controller');

exports.getAllBoards = async (req, res) => {
  const boards = await Board.find({});
  // console.log('boards: ', boards)
  res.status(200).json({ boards })
}
// https://stackoverflow.com/questions/42974735/create-object-from-array

function resolveUsers(users) {
  return Promise.all(
    users.map(async user => {
      const userArray = await User.find(
        { _id: user.userId }, 
        { _id: 1, email: 1, firstname: 1, lastname: 1 }
      )
      return userArray[0];
    })
  );
}

/*
  * Mongodb keys are not human readable, by converting toJSON, the keys are readable
  * and that is what sanitizedBoardObject does
*/
function convertObjKeys(obj) {
  // console.log('obj ', obj)
  // if(array[0] === undefined) return;
  return Object.keys(obj.toJSON()).reduce((o, key) => ({ ...o, [key]: obj[key]}), {})
}

exports.getBoardById = async (req, res) => {
  const { boardId } = req.params;
  // console.log('boardID: ', boardId)
  const board = await Board.find({ _id: boardId });
  // console.log('board: ', { boardId, board })
  const [{ owners, collab }] = board;
  // console.log({ owners, collab })
  // get user info
  const resolvedOwners = await resolveUsers(owners);
  const resolvedCollab = await resolveUsers(collab);
  // console.log('line 31 :', { boardId, resolvedOwners, resolvedCollab, board })
  const boardMapped = convertObjKeys(board[0]);
  const ownersMapped = await resolvedOwners.map(owner => convertObjKeys(owner))
  const collabMapped = await resolvedCollab.map(collab => convertObjKeys(collab)) 

  boardMapped.owners = ownersMapped;
  boardMapped.collab = collabMapped;
  
  res.status(200).json({ board: boardMapped }) 
}

exports.createBoard = async (req, res) => {
  // console.log('req.body: ', req.body)
  const { title, userId } = req.body;
  const board = new Board({
    title: req.body.title,
    owners: [{ userId: userId }],
  });

  board.save(async(err, board) => {
    if(err) {
      res.status(500).send({ message: err });
      return;
    }
    // console.log('board: ', board)
    const boardId = board._id;
    const userArray = await User.find({ _id: req.body.userId });
    const user = userArray[0];
    // console.log('user: ', user);
    user.ownBoards.push({ boardId: boardId })
    /*const user = await User.
      findOneAndUpdate(
        { _id: req.body.userId }, 
        { $push: { ownBoards: {boardID: boardID }}}, 
        {
          new: true,
          runValidators: true,
          returnDocument: 'after'
        });*/
    const saved = await user.save();
    // console.log('saved: ', saved)
    // console.log('board: ', board)
    res.status(200).send({
      message: 'Board and User record updated successfully!',
      user: saved,
      board: board
    })
  })
}

exports.addOwnership = async (req, res) => {
  const { boardId } = req.params;
  const { userId } = req.body;
  // add user to owner's array
  const addUserToOwners = await Board.findOneAndUpdate({ _id: boardId }, { $push: { owners: { userId: userId }}}, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  });

  // remove user from collab array;
  const removeUserFromCollab = await Board.findOneAndUpdate({ _id: boardId }, 
    { $pull: { collab: { userId: userId }} }, 
    { 
      multi: true,
      // returnDocument: 'after'
    } 
  );

  res.status(200).send({ board: removeUserFromCollab });
}

exports.removeOwnership = async (req, res) => {
  const { boardId } = req.params;
  const { userId } = req.body;
  // add user to owner's array
  const addUserToCollab = await Board.findOneAndUpdate({ _id: boardId }, { $push: { collab: { userId: userId }}}, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  });

  // remove user from collab array;
  const removeUserFromOwners = await Board.findOneAndUpdate({ _id: boardId }, 
    { $pull: { owners: { userId: userId }} }, 
    { 
      multi: true,
      // returnDocument: 'after'
    } 
  );

  res.status(200).send({ board: removeUserFromOwners });
}

exports.deleteBoard = async (req, res) => {
  const { boardId } = req.params;
  const { userId } = req.body;
  const board = await Board.deleteOne({ _id: boardId })
  const array = await User.find({ _id: userId });
  const user = array[0];
  const userBoardList = user.ownBoards;
  const updatedBoardList = userBoardList.filter(board => board.boardId !== boardId)
  user.ownBoards = updatedBoardList;
  const saved = await user.save();
  res.status(200).send({
    message: 'Board is deleted from board and user records',
    user: user,
    board: board
  })
}

exports.editBoardTitle = async (req, res) => {
  const { boardId } = req.params;
  const { title } = req.body;
  // console.log('boardId: ', boardId)
  // console.log('title: ', title)
  const board = await Board.findOneAndUpdate( { _id: boardId }, { $set: { title: title } }, 
    {
      new: true,
      runValidators: true,
      returnDocument: 'after'
    })
  // console.log('board: ', board)
  res.status(201).json({ board });
}

exports.addListToBoard = async (req, res) => {
  const { boardId } = req.params;
  const { title: name } = req.body;
  const board = await Board.findOneAndUpdate({ _id: boardId }, { $push: { list: { name: name }}}, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  })

  // console.log('board ', board)
  res.status(201).json({ board });
}

exports.changeBoardListPosition = async (req, res) => {
  const { boardId } = req.params;
  const { currentListIndex, newListIndex } = req.body;

  const boardArray = await Board.find({ _id: boardId });
  const list = boardArray[0].list;
  // swap index positions
  const temp = list[currentListIndex];
  list[currentListIndex] = list[newListIndex];
  list[newListIndex] = temp;

  const board = boardArray[0];
  const updatedBoard = await board.save();
  res.status(201).json({ updatedBoard });
}

exports.addCollab = async (req, res) => {
  const { boardId } = req.params;
  const { email, userId } = req.body;
  console.log('addCollab: ', req.body)
  const board = await Board.find({ _id: boardId });
  // console.log('board; ',board)
  const [{ collab }] = board;
  // console.log('collab: ', collab)
  const emailFound = collab.filter((collab) => collab.email === email);
  if(emailFound.length > 0) {
    console.log('Email exist')
    return;
  }
  const updateBoard = await Board.findOneAndUpdate(
    { _id: boardId }, 
    { $push: { collab: { email: email, userId: userId, pending: true } }});
  console.log('board: ', board)
  res.status(200).json({ board: updateBoard });
}

exports.removeCollab = async (req, res) => {
  const { boardId } = req.params;
  const { userId, key } = req.body;
  console.log('req.body: ', req.body)
  const documentKey = (key === "owners") ? 'owners' : 'collab';
console.log('documentKey: ', documentKey)
  const board = await Board.findOneAndUpdate({ _id: boardId }, 
    { $pull: { [documentKey]: { userId: userId }} }, 
    { 
      multi: true,
      returnDocument: 'after'
    } 
  );
  console.log('remove collab: ', board)
  // MailControl.notifyRemoval()
  res.status(200).json({ msg: "successfully removed user from collab list" })
}

exports.editBoardListTitle = async (req, res) => {
  const { boardId, listId } = req.params;
  const { title } = req.body;

  const board = await Board.findOneAndUpdate({ _id: boardId, "list._id": listId },  { $set: { "list.$.name": title } }, {
    new: true,
    runValidators: true,
    returnDocument: 'after'
  })

  res.status(201).json({ board });
}

exports.deleteListFromBoard = async (req, res) => {
  const { boardId, listId } = req.params;

  const board = await Board.findOneAndUpdate(
    { _id: boardId }, 
    {$pull: { list: { _id: listId } }},
    {
      returnDocument: 'after'
    })

  if(!board) {
    // console.log('No board id: ')
    res.status(404).json({ failed: 'no label' });
  }

  // console.log('board ', board)
  res.status(201).send({ board });
}
