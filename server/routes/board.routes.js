const express = require('express');
const router = express.Router();
const { authJwt } = require('../middleware');
const BoardControl = require('../controller/board.controller');

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.post('/create', [authJwt.verifyToken], BoardControl.createBoard);
router.delete(
  '/:boardId/:listId/delete-boardlist', 
  [authJwt.verifyToken], 
  BoardControl.deleteListFromBoard
)
router.patch(
  '/:boardId/:listId/edit-boardlist-title', 
  [authJwt.verifyToken], 
  BoardControl.editBoardListTitle
)
router.patch(
  '/:boardId/change-boardlist-position', 
  [authJwt.verifyToken], 
  BoardControl.changeBoardListPosition
)
router.patch(
  '/:boardId/add-boardlist', 
  [authJwt.verifyToken], 
  BoardControl.addListToBoard
)
router.patch('/:boardId/add-collab',
  [authJwt.verifyToken],
  BoardControl.addCollab
)
router.patch('/:boardId/remove-collab',
  [authJwt.verifyToken],
  BoardControl.removeCollab
)
router.patch('/:boardId/add-ownership',
  [authJwt.verifyToken],
  BoardControl.addOwnership
)
router.patch('/:boardId/remove-ownership',
  [authJwt.verifyToken],
  BoardControl.removeOwnership
)
router.patch('/:boardId/edit-title', [authJwt.verifyToken], BoardControl.editBoardTitle)
router.post('/:boardId/delete', [authJwt.verifyToken], BoardControl.deleteBoard)
router.get('/:boardId', [authJwt.verifyToken], BoardControl.getBoardById)
router.get('/', [authJwt.verifyToken], BoardControl.getAllBoards)


module.exports = router;
