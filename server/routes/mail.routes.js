const express = require('express');
const router = express.Router();
const { authJwt } = require('../middleware');
const MailControl = require('../controller/mail.controller');

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.post('/', [authJwt.verifyToken], MailControl.sendMail)

module.exports = router;