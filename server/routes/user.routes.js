const express = require('express');
const router = express.Router();
const { authJwt } = require('../middleware');
const UserControl = require('../controller/user.controller');

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

// https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes
router.patch(
  '/:userId/add-board', 
  [authJwt.verifyToken], 
  UserControl.addBoardToUserRecord
)
router.patch(
  '/:userId/remove-board', 
  [authJwt.verifyToken], 
  UserControl.removeBoardFromUserRecord
)
router.get('/:userId', [authJwt.verifyToken], UserControl.getUserBoards)
// router.patch('/', UserControl.editUserModelOwnBoards)
router.post('/', [authJwt.verifyToken], UserControl.getUsers)
router.get('/', [authJwt.verifyToken], UserControl.getAllUserProfile)

module.exports = router;
