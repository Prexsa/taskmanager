const express = require('express');
const router = express.Router();
const { authJwt } = require('../middleware');
const TaskControl = require('../controller/task.controller');

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

// router.patch('/:taskId', TaskControl.editDbModelKey);
router.patch('/:taskId', [authJwt.verifyToken], TaskControl.updateTask)
router.delete('/:taskId', [authJwt.verifyToken], TaskControl.deleteTask)
router.patch('/:boardId/:taskId', [authJwt.verifyToken], TaskControl.addFieldToTask)
router.get('/:boardId', [authJwt.verifyToken], TaskControl.getAllTasksByBoardId)
router.patch('/', [authJwt.verifyToken], TaskControl.updateManyTasksToNewListId)
router.post('/', [authJwt.verifyToken], TaskControl.createTask)
router.get('/', [authJwt.verifyToken], TaskControl.getAllTasks)

module.exports = router;
