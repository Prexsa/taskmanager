const express = require('express');
const router = express.Router();
const { verifySignUp } = require('../middleware');
const AuthControl = require('../controller/auth.controller');

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.route('/check-email').post(AuthControl.checkEmail)
router.route('/collab-register').post(AuthControl.collabRegister)
// router.route('/signup').post(verifySignUp.checkDuplicateEmail, AuthControl.signup)
router.route('/signup').post(AuthControl.signup)
router.route('/signin').post(AuthControl.signin)
router.route('/forgot-password').post(AuthControl.updatePassword)

module.exports = router;