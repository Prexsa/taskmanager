const mongoose = require('mongoose');

const CollabModel = new mongoose.Schema({
  boardId: String,
  collabs: [{ email: String }]
});

const Collab = mongoose.model("Collab", CollabModel);
module.exports = Collab;