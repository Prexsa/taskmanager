const mongoose = require('mongoose');

const TaskListSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  boardId: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  position: {
    type: String,
  }
})

const TaskList = mongoose.model('TaskList', TaskListSchema);
module.exports = TaskList;