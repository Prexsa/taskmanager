const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  firstname: {
    type: String,
    required: true,
  },
  lastname: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  ownBoards: [
    { 
      boardId: String,
      collab: Boolean,
    }
  ]
});

const User = mongoose.model("User", UserSchema);
module.exports = User;
