const mongoose = require('mongoose');

const BoardSchema = new mongoose.Schema({
  title: String,
  list: [{ name: String }],
  owners: [{ userId: String }],
  collab: [{ userId: String, email: String, pending: Boolean }],
  date: {
    type: Date,
    default: Date.now
  }
});

const Board = mongoose.model("Board", BoardSchema);
module.exports = Board;