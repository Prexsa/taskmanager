require('dotenv').config();
const express = require('express');
const cors = require('cors');
const app = express();
const connectDB = require('./db/connect')
const logger = require('morgan');

// to keep track of all the requests
// app.use(logger('common'));

// middleware needs to be before routes
// parse form data
app.use(express.urlencoded({ extended: false }));
// parse json
app.use(express.json());
app.use(cors());

// import routes
// require('./routes/auth.routes')(app);
const auth = require('./routes/auth.routes');
const user = require('./routes/user.routes');
const board = require('./routes/board.routes');
const task = require('./routes/task.routes');
const mail = require('./routes/mail.routes');

// routes
app.use('/api/v1/auth', auth)
app.use('/api/v1/user', user)
app.use('/api/v1/board', board)
app.use('/api/v1/task', task)
app.use('/api/v1/mail', mail)

app.get('/', (req, res) => {
  res.send('Hello World!')
})

const PORT = process.env.PORT || 9001;
// const DATABASE_URI = process.env.MONGOLAB_URI;
const DATABASE_URI = process.env.MONGO_URI
// const DATABASE_URI = process.env.MONGOLAB_ATLAS;

const start = async () => {
  try {
    const conn = await connectDB(DATABASE_URI);
    console.log(`MongoDB connected: ${conn.connection.host}`);

    app.listen(PORT, 
      () => console.log(`Server listening on PORT ${PORT}`)
    );
  } catch (error) {
    console.log('Start Error: ', error)
  }
}

start();



// https://blog.logrocket.com/building-structuring-node-js-mvc-application/
