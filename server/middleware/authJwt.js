const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;

verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  // console.log('token: ', token)
  if(!token) {
    return res.status(403).send({ message: 'No token provided!' });
  }
  jwt.verify(token, JWT_SECRET, (err, decoded) => {
    if(err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    req.userId = decoded.id;
    next();
  });
};

const authJwt = {
  verifyToken
};
module.exports = authJwt;
