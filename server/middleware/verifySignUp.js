const User = require('../models/user.model');

checkDuplicateEmail = (req, res, next) => {
  // console.log('req: ', req.body)
  User.findOne({
    email: req.body.email
  }).exec((err, user) => {
    console.log('user: ', user)
    if(err) {
      res.status(500).send({ message: err });
      return;
    }
    if(user) {
      console.log('success user')
      res.status(400).send({ message: 'Failed! Email is already in use!' });
      return;
    }
    console.log('success next')
    next();
  })
}

const verifySignUp = {
  checkDuplicateEmail
};
module.exports = verifySignUp;